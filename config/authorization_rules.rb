authorization do
  role :admin do
    has_permission_on [:policynames], :to => [:index, :show, :new, :create, :edit, :update, :destroy,:print,:export]
    has_permission_on [:users], :to => [:index, :show, :new, :create, :edit, :update, :destroy,:print,:export,:profile,:document,:upload_document,:editprofile,:updateprofile,:upload,:csv_import,:changepassword,:updatepassword,:resetpass,:payment_mode,:activity]
    has_permission_on [:insurances], :to => [:index,:show,:edit,:update,:suspense_transactions]
    has_permission_on [:tickets], :to=>[:index, :show,:edit,:update]
    has_permission_on [:pancards], :to=>[:index, :show,:edit,:update]
    has_permission_on [:deposits], :to=>[:index,:show,:update_funds,:upload_funds,:upload,:fund_save,:suspense_transactions]
    has_permission_on [:reports], :to=>[:users,:user_report,:insurances,:insurance_export,:insurance_eodreport,:insurance_eodreport_export,:insurance_daterangereport,:insurance_daterangexport,:user_activity,:user_district_report,:user_district_export,:insurance_eodpayment,:insurance_datepayment,:user_log_report,:user_log_export,:ins_suspense_transactions,:insu_suspense_export,:insu_suspense_collection,:user_accounts,:account_statement]
    has_permission_on [:reports], :to=>[:pancards,:pancard_export,:pancard_eodreport,:pancard_eodreport_export,:pancard_daterangereport,:pancard_daterangexport,:pancard_eodpayment,:pancard_datepayment]
    has_permission_on [:reports], :to=>[:funds,:fund_eodreport,:fund_eodreport_export,:fund_export,:fund_daterangereport,:fund_daterangexport,:fund_eodpayment,:fund_datepayment]
    has_permission_on [:reports], :to=>[:educations,:education_eodreport,:education_eodreport_export,:education_daterangereport,:education_daterangexport,:education_eodpayment,:education_datepayment,:education_export,:show]
    has_permission_on [:bulkmails] , :to=>[:index,:new,:create,:show,:mail_mode]
    has_permission_on [:downloads],:to=>[:index,:new,:create,:show]
    has_permission_on [:courses],:to=>[:index,:show,:new,:create,:edit,:update]
    has_permission_on [:deducations],:to=>[:index,:show]
    has_permission_on [:universities], :to=>[:index,:show,:new,:create]
    has_permission_on [:shortmessages], :to=>[:index,:show,:export]
    has_permission_on [:user_info],:to=>[:index]
  end
 
  role :super_admin do
    has_permission_on [:states,:divisions,:districts,:blocks,:roles], :to => [:index, :show, :new, :create, :edit, :update,:destroy,:export]
    has_permission_on [:users], :to => [:index, :show, :new, :create, :edit, :update,:destroy,:verifyuser,:update_status,:changepassword,:updatepassword,:profile,:updateprofile,:editprofile]

  end
  
  role :manager do
    has_permission_on [:users], :to => [:show,:verifyuser,:update_status,:index,:profile,:editprofile,:updateprofile,:changepassword,:updatepassword,:promote]
    has_permission_on [:insurances], :to => [:index,:verifyinsurance,:show,:update_status,:suspense,:suspense_transactions]
    has_permission_on [:pancards], :to=>[:index, :show,:update_status,:suspense,:suspense_transactions]
    has_permission_on [:deposits], :to=>[:index,:show,:update_status,:suspense,:suspense_transactions]
    has_permission_on [:deducations],:to=>[:index,:show,:update_status]
    has_permission_on [:downloads],:to=>[:index,:show]
    has_permission_on [:reports], :to=>[:users,:user_report,:insurances,:insurance_export,:insurance_eodreport,:insurance_eodreport_export,:insurance_daterangereport,:insurance_daterangexport,:user_activity,:user_district_report,:user_district_export,:insurance_eodpayment,:insurance_datepayment,:user_log_report,:user_log_export,:ins_suspense_transactions,:insu_suspense_export,:insu_suspense_collection]
    has_permission_on [:reports], :to=>[:pancards,:pancard_export,:pancard_eodreport,:pancard_eodreport_export,:pancard_daterangereport,:pancard_daterangexport,:pancard_eodpayment,:pancard_datepayment]
    has_permission_on [:reports], :to=>[:funds,:fund_eodreport,:fund_eodreport_export,:fund_export,:fund_daterangereport,:fund_daterangexport,:fund_eodpayment,:fund_datepayment,:account_statement]
    has_permission_on [:reports], :to=>[:educations,:education_eodreport,:education_eodreport_export,:education_daterangereport,:education_daterangexport,:education_eodpayment,:education_datepayment,:education_export]

  end


  role :vle do
    has_permission_on [:insurances], :to => [:index,:show,:new,:print,:export,:create,:courier,:courier_update,:receipt,:verifiedinsurance,:renewpolicy,:createrenew,:payment_mode,:suspense_transactions]
    has_permission_on [:users], :to => [:profile,:editprofile,:updateprofile,:changepassword,:updatepassword]
    has_permission_on [:tickets], :to=>[:index, :show,:new,:create,:update_status]
    has_permission_on [:pancards], :to=>[:index, :show,:new,:create,:show,:payment_mode,:print]
    has_permission_on [:deposits], :to=>[:index,:new,:create,:show,:transaction_mode,:suspense_transactions]
    has_permission_on [:downloads],:to=>[:index]
    has_permission_on [:deducations],:to=>[:index,:new,:create,:payment_mode,:show,:get_amount,:load_courses,:print]
    has_permission_on [:courses],:to=>[:index,:show]
    has_permission_on [:reports],:to=>[:account_statement]
  end

  role :dlf_agent do
    has_permission_on [:insurances], :to => [:index,:show,:new,:print,:export,:create,:courier,:courier_update,:receipt,:verifiedinsurance,:renewpolicy,:createrenew,:payment_mode,:suspense_transactions]
    has_permission_on [:users], :to => [:profile,:editprofile,:updateprofile,:changepassword,:updatepassword]
    has_permission_on [:deposits], :to=>[:index,:new,:create,:show,:transaction_mode,:suspense_transactions]
  end

  role :district_manager do
    has_permission_on [:users], :to => [:index,:show]
    has_permission_on [:tickets], :to =>[:index,:show]
    has_permission_on [:pancards], :to =>[:index,:show]
    has_permission_on [:insurances], :to=>[:index,:show]
  end
  role :district_engineer do
    has_permission_on [:users], :to => [:index,:show]
    has_permission_on [:tickets], :to =>[:index,:show]
    has_permission_on [:pancards], :to =>[:index,:show]
    has_permission_on [:insurances], :to=>[:index,:show]
  end

  role :state_engineer do
    has_permission_on [:tickets], :to =>[:index,:show,:update,:edit]
    has_permission_on [:users], :to=>[:profile,:editprofile,:updateprofile,:changepassword,:updatepassword]
  end

end
