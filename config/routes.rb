Csc::Application.routes.draw do
  resources :shortmessages do
    get :export, :on=>:collection
  end

  resources :universities

  resources :deducations do
    get :payment_mode, :on=>:collection
    get :get_amount, :on=>:collection
    get :update_status, :on=>:member
    get :print, :on=>:member
  end

  resources :courses

  resources :downloads

  resources :bulkmails do
    get :mail_mode, :on=>:collection
  end

  resources :deposits do
    get :transaction_mode, :on=>:collection
    get :update_status, :on=>:member
    get :update_funds, :on=>:member
    get :upload_funds, :on=>:collection
    get :upload, :on=>:member
    put :fund_save, :on=>:collection
    get :suspense_transactions, :on=>:collection
  end

  resources :pancards do
    get :update_status, :on=>:member
    get :payment_mode, :on=>:collection
    get :print, :on=>:member
    get :suspense_transactions, :on=>:collection
  end

  resources :tickets

  resources :policynames

  resources :insurances do
    get 'export', :on=>:collection
    get 'print', :on=>:member
    get 'receipt', :on=>:member
    get 'courier', :on=>:member
    post 'courier_update', :on=>:member
    get 'verifyinsurance', :on=>:collection
    get 'update_status', :on=>:member
    get 'verifiedinsurance', :on=>:collection
    get 'renewpolicy', :on=>:collection
    put 'createrenew', :on=>:collection
    get :eodreport, :on=>:collection
    get :daterangereport, :on=>:collection
    get :eodexport, :on=>:collection
    get :daterangeexport, :on=>:collection
    get :payment_mode, :on=>:collection
    get :suspense_transactions, :on=>:collection
  end

  resources :user_profiles

  resources :blocks do
    get 'load_districts', :on => :collection
    get :export, :on=>:collection
  end

  resources :divisions do
    get :export, :on=>:collection
  end

  resources :districts do
    get :export, :on => :collection
  end

  resources :states do
    get :export, :on=>:collection
  end

  resources :users do
    get 'verifyuser', :on=>:collection
    get 'update_status', :on=>:member
    get 'profile', :on=>:member
    get 'document', :on=>:member
    put 'upload_document', :on=>:member
    get 'editprofile', :on=>:member
    put 'updateprofile', :on=>:member
    get 'upload', :on=>:collection
    post 'csv_import', :on=>:collection
    get 'changepassword', :on=>:member
    get :resetpass, :on=>:member
    get :payment_mode, :on=>:collection
    get :activity, :on=>:member
    get :export, :on=>:collection
  end

  resources :roles

  resources :user_sessions

  match 'login' => "user_sessions#new", :as => :login
  match 'logout' => "user_sessions#destroy", :as => :logout

  root :to => "user_sessions#new"

  match "user_info/:id" => "user_info#index", :as=>:user_info
  get "user_info/index"


  match ':controller(/:action(/:id(.:format)))'
end
