class CreateDeducations < ActiveRecord::Migration
  def self.up
    create_table :deducations do |t|

      t.date     :transaction_date
      t.string   :transaction_id
      t.string   :application_number
      t.integer  :university_id
      t.integer  :course_id
      t.string   :name
      t.string   :father_name
      t.string   :gender
      t.string   :marital_status
      t.string   :religion
      t.boolean  :ph_status,:default=>0
      t.boolean  :bpl, :default=>0
      t.date     :dob
      t.string   :address1
      t.string   :address2
      t.string   :address3
      t.string   :place
      t.integer  :pin
      t.integer  :state_id
      t.integer  :district_id
      t.string  :mobile
      t.string   :email

      t.string   :qualification1
      t.string   :year1
      t.string   :percentage1
      t.string   :board1

      t.string   :qualification2
      t.string   :year2
      t.string   :percentage2
      t.string   :board2

      t.string   :qualification3
      t.string   :year3
      t.string   :percentage3
      t.string   :board3

      t.string   :qualification4
      t.string   :year4
      t.string   :percentage4
      t.string   :board4

      t.string   :technical_skill
      t.boolean   :employee_status, :default=>0
      t.text     :remarks
      t.integer  :user_id
      t.boolean  :status, :default=>0
      t.integer  :amount
      t.string   :payment_mode
      t.string   :bank_name
      t.string   :branch
      t.string   :instrument_number
      t.date     :instrument_date

      t.integer  :created_by
      t.integer  :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :deducations
  end
end
