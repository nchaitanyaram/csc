class Dbupdates < ActiveRecord::Migration
  def self.up

    add_column :insurances, :suspense_status,    :boolean, :default => 0
    add_column :pancards, :suspense_status,    :boolean, :default => 0
    add_column :deducations, :suspense_status,    :boolean, :default => 0
    add_column :deposits, :suspense_status,    :boolean, :default => 0

  end

  def self.down
    remove_column :insurances, :suspense_status
    remove_column :pancards, :suspense_status
    remove_column :deducations, :suspense_status
    remove_column :deposits, :suspense_status
  end
end
