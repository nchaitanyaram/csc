#Author: Chaitanya
#Date: June 04 2011
#==========================================
class CreateInsurances < ActiveRecord::Migration
  def self.up
    create_table :insurances do |t|
      
      t.string :transaction_id
      t.date :transaction_date
      t.string :policy_type          #New/Renewal
      t.integer :policyname_id       #Policy Name
      t.string :application_number
      t.integer :sum_assured
      t.integer :annual_premium
      t.integer :modal_premium
      t.string :payment_frequency    #Annual/Half Yearly etc.
      t.integer :collected_premium
      t.string  :payment_mode
      t.string  :bank_name
      t.string  :branch_name
      t.integer :instrument_number
      t.date    :instrument_date
      t.integer :age
      t.integer :term                #policy term ie 20,10 etc
      t.string :policy_holder
      t.string :insured_holder
      t.string :gender
      t.date :dob
      t.integer :state_id
      t.integer :district_id
      t.string :address1
      t.string :address2
      t.string :place
      t.integer :pin
      t.string :mobile
      t.string :telephone
      t.date :physical_date          #Courier Dispath Date
      t.string :courier              #courier Name
      t.string :awb_number           #Tracking Number(Airwaybill)
      t.string :policy_number
      t.text   :remarks
      t.boolean :status, :default=>0

      t.integer :user_id
      t.integer :created_by
      t.integer :updated_by     

      t.timestamps
    end
  end

  def self.down
    drop_table :insurances
  end
end
