class CreateTickets < ActiveRecord::Migration
  def self.up
    create_table :tickets do |t|

      t.string  :ticket_number
      t.date    :open_date
      t.integer :user_id
      t.string  :ticket_type
      t.text    :ticket_description
      t.text    :problem_solution
      t.date    :close_date
      t.integer :created_by
      t.integer :updated_by
      t.boolean :status, :default=>0
      
      
      t.timestamps
    end
  end

  def self.down
    drop_table :tickets
  end
end
