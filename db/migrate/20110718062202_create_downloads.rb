class CreateDownloads < ActiveRecord::Migration
  def self.up
    create_table :downloads do |t|
      t.string :name
      t.text :description
# file upload
      t.string  :upload_file_name
      t.string  :upload_content_type
      t.integer :upload_file_size
      t.datetime :upload_updated_at
####
      t.integer  :created_by
      t.integer  :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :downloads
  end
end
