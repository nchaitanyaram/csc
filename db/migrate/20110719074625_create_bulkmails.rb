class CreateBulkmails < ActiveRecord::Migration
  def self.up
    create_table :bulkmails do |t|
      t.date :mail_date
      t.text :subject
      t.text :message
      t.string :mail_type
      t.integer :district_id
      t.text    :to_cc

      t.integer :created_by
      t.integer :updated_by
      t.timestamps
    end
  end

  def self.down
    drop_table :bulkmails
  end
end
