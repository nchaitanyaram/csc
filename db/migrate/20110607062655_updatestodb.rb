#Author: Chaitanya
#Add Extra fields to User for Avatar
#=====================================================
class Updatestodb < ActiveRecord::Migration
  def self.up
    add_column :users, :avatar_file_name,    :string
    add_column :users, :avatar_content_type, :string
    add_column :users, :avatar_file_size,    :integer
    add_column :users, :avatar_updated_at,   :datetime
    
#    #User Profile - ID Proof,Address Proof and Agreement - Upload
    add_column :user_profiles, :idproof_file_name,    :string
    add_column :user_profiles, :idproof_content_type, :string
    add_column :user_profiles, :idproof_file_size,    :integer
    add_column :user_profiles, :idproof_updated_at,   :datetime

    add_column :user_profiles, :addressproof_file_name,    :string
    add_column :user_profiles, :addressproof_content_type, :string
    add_column :user_profiles, :addressproof_file_size,    :integer
    add_column :user_profiles, :addressproof_updated_at,   :datetime
    
    add_column :user_profiles, :agreement1_file_name,    :string
    add_column :user_profiles, :agreement1_content_type, :string
    add_column :user_profiles, :agreement1_file_size,    :integer
    add_column :user_profiles, :agreement1_updated_at,   :datetime

    add_column :user_profiles, :agreement2_file_name,    :string
    add_column :user_profiles, :agreement2_content_type, :string
    add_column :user_profiles, :agreement2_file_size,    :integer
    add_column :user_profiles, :agreement2_updated_at,   :datetime

    add_column :user_profiles, :agreement3_file_name,    :string
    add_column :user_profiles, :agreement3_content_type, :string
    add_column :user_profiles, :agreement3_file_size,    :integer
    add_column :user_profiles, :agreement3_updated_at,   :datetime

    add_column :user_profiles, :agreement4_file_name,    :string
    add_column :user_profiles, :agreement4_content_type, :string
    add_column :user_profiles, :agreement4_file_size,    :integer
    add_column :user_profiles, :agreement4_updated_at,   :datetime

    add_column :user_profiles, :agreement5_file_name,    :string
    add_column :user_profiles, :agreement5_content_type, :string
    add_column :user_profiles, :agreement5_file_size,    :integer
    add_column :user_profiles, :agreement5_updated_at,   :datetime

    add_column :user_profiles, :agreement6_file_name,    :string
    add_column :user_profiles, :agreement6_content_type, :string
    add_column :user_profiles, :agreement6_file_size,    :integer
    add_column :user_profiles, :agreement6_updated_at,   :datetime

    add_column :user_profiles, :agreement7_file_name,    :string
    add_column :user_profiles, :agreement7_content_type, :string
    add_column :user_profiles, :agreement7_file_size,    :integer
    add_column :user_profiles, :agreement7_updated_at,   :datetime

    add_column :user_profiles, :agreement8_file_name,    :string
    add_column :user_profiles, :agreement8_content_type, :string
    add_column :user_profiles, :agreement8_file_size,    :integer
    add_column :user_profiles, :agreement8_updated_at,   :datetime




  end
  
  def self.down
    remove_column :users, :avatar_file_name
    remove_column :users, :avatar_content_type
    remove_column :users, :avatar_file_size
    remove_column :users, :avatar_updated_at

    remove_column :user_profiles, :idproof_file_name,    :string
    remove_column :user_profiles, :idproof_content_type, :string
    remove_column :user_profiles, :idproof_file_size,    :integer
    remove_column :user_profiles, :idproof_updated_at,   :datetime

    remove_column :user_profiles, :addressproof_file_name,    :string
    remove_column :user_profiles, :addressproof_content_type, :string
    remove_column :user_profiles, :addressproof_file_size,    :integer
    remove_column :user_profiles, :addressproof_updated_at,   :datetime
    
    remove_column :user_profiles, :agreement1_file_name,    :string
    remove_column :user_profiles, :agreement1_content_type, :string
    remove_column :user_profiles, :agreement1_file_size,    :integer
    remove_column :user_profiles, :agreement1_updated_at,   :datetime

    remove_column :user_profiles, :agreement2_file_name,    :string
    remove_column :user_profiles, :agreement2_content_type, :string
    remove_column :user_profiles, :agreement2_file_size,    :integer
    remove_column :user_profiles, :agreement2_updated_at,   :datetime

    remove_column :user_profiles, :agreement3_file_name,    :string
    remove_column :user_profiles, :agreement3_content_type, :string
    remove_column :user_profiles, :agreement3_file_size,    :integer
    remove_column :user_profiles, :agreement3_updated_at,   :datetime

    remove_column :user_profiles, :agreement4_file_name,    :string
    remove_column :user_profiles, :agreement4_content_type, :string
    remove_column :user_profiles, :agreement4_file_size,    :integer
    remove_column :user_profiles, :agreement4_updated_at,   :datetime

    remove_column :user_profiles, :agreement5_file_name,    :string
    remove_column :user_profiles, :agreement5_content_type, :string
    remove_column :user_profiles, :agreement5_file_size,    :integer
    remove_column :user_profiles, :agreement5_updated_at,   :datetime

    remove_column :user_profiles, :agreement6_file_name,    :string
    remove_column :user_profiles, :agreement6_content_type, :string
    remove_column :user_profiles, :agreement6_file_size,    :integer
    remove_column :user_profiles, :agreement6_updated_at,   :datetime

    remove_column :user_profiles, :agreement7_file_name,    :string
    remove_column :user_profiles, :agreement7_content_type, :string
    remove_column :user_profiles, :agreement7_file_size,    :integer
    remove_column :user_profiles, :agreement7_updated_at,   :datetime

    remove_column :user_profiles, :agreement8_file_name,    :string
    remove_column :user_profiles, :agreement8_content_type, :string
    remove_column :user_profiles, :agreement8_file_size,    :integer
    remove_column :user_profiles, :agreement8_updated_at,   :datetime

    remove_column :pancards, :idproof_file_name,    :string
    remove_column :pancards, :idproof_content_type, :string
    remove_column :pancards, :idproof_file_size,    :integer
    remove_column :pancards, :idproof_updated_at,   :datetime
  end
end
