class CreateDeposits < ActiveRecord::Migration
  def self.up
    create_table :deposits do |t|
      t.integer  :user_id
      t.string   :transaction_number
      t.date     :transaction_date
      t.string   :transaction_mode   ## Cash, DD, Cheque,online
      t.string   :transaction_type   ## Debit or Credit 
      t.string   :reference_number
      t.string   :bank_name
      t.string   :branch
      t.integer  :instrument_number
      t.date     :instrument_date
      t.float    :amount
      t.boolean  :status, :default=>0
      t.boolean  :fundstatus, :default=>0
      
      t.text     :remarks
# file upload for proof of Deposit
      t.string  :proof_file_name
      t.string  :proof_content_type
      t.integer :proof_file_size
      t.datetime :proof_updated_at
####      
      t.integer  :created_by
      t.integer  :updated_by
      
      t.timestamps
    end
  end

  def self.down
    drop_table :deposits
  end
end
