class CreatePolicynames < ActiveRecord::Migration
  def self.up
    create_table :policynames do |t|
      t.string :name
      t.text   :description
      t.integer :min_age
      t.integer :max_age
      t.float   :min_amount
      t.float   :max_amount
      t.integer :min_term
      t.integer :max_term
      
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end

  def self.down
    drop_table :policynames
  end
end
