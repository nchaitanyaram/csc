class CreateTransactions < ActiveRecord::Migration
  def self.up
    create_table :transactions do |t|
      t.integer  :user_id
      t.string   :transaction_number
      t.date     :transaction_date
      t.string   :service
      t.string   :transaction_mode
      t.float    :amount
      t.boolean  :status, :default=>0
      t.integer  :created_by
      t.integer  :updated_by
      
      t.timestamps
    end
  end

  def self.down
    drop_table :transactions
  end
end
