class CreateCourses < ActiveRecord::Migration
  def self.up
    create_table :courses do |t|
      t.string   :name
      t.integer   :university_id
      t.text     :description
      t.text     :eligibility
      t.integer  :fee
      t.string   :duration

      t.integer  :created_by
      t.integer  :modified_by

      t.timestamps
    end
  end

  def self.down
    drop_table :courses
  end
end
