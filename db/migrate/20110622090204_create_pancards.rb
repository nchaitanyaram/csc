class CreatePancards < ActiveRecord::Migration
  def self.up
    create_table :pancards do |t|
      t.string  :panapplication_number
      t.date    :transaction_date
      t.string  :first_name
      t.string  :middle_name
      t.string  :last_name
      t.date    :dob
      t.string  :gender
      t.string  :father_name
      t.text    :address1
      t.text    :address2
      t.text    :address3
      t.string  :place
      t.integer  :district_id
      t.integer  :state_id
      t.integer :pincode
      t.text    :mobile
      t.text    :telephone
      t.float :amount
      t.string :payment_mode
      t.string :instrument_number
      t.date :instrument_date
      t.string :bank_name
      t.string :branch

      t.string  :idproof_file_name
      t.string  :idproof_content_type
      t.integer :idproof_file_size
      t.datetime :idproof_updated_at

      t.string  :addressproof_file_name
      t.string  :addressproof_content_type
      t.integer :addressproof_file_size
      t.datetime :addressproof_updated_at

      t.string  :dobproof_file_name
      t.string  :dobproof_content_type
      t.integer :dobproof_file_size
      t.datetime :dobproof_updated_at

      t.text    :remarks
      
      t.boolean :status, :default=>0
      t.integer :user_id
      t.integer :created_by
      t.integer :updated_by
      
      t.timestamps
    end
  end

  def self.down
    drop_table :pancards
  end
end
