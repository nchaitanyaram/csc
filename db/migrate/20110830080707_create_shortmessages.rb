class CreateShortmessages < ActiveRecord::Migration
  def self.up
    create_table :shortmessages do |t|

      t.integer :user_id
      t.string  :transaction_id
      t.string  :service
      t.text    :message
      t.string  :mobile
      t.string  :msgid
      t.string  :status

      t.integer :created_by
      t.integer :updated_by


      t.timestamps
    end
  end

  def self.down
    drop_table :shortmessages
  end
end
