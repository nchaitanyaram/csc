# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111230064215) do

  create_table "assignments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blocks", :force => true do |t|
    t.integer  "state_id"
    t.integer  "division_id"
    t.integer  "district_id"
    t.string   "name"
    t.text     "description"
    t.text     "short_code"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bulkmails", :force => true do |t|
    t.date     "mail_date"
    t.text     "subject"
    t.text     "message"
    t.string   "mail_type"
    t.integer  "district_id"
    t.text     "to_cc"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", :force => true do |t|
    t.string   "name"
    t.integer  "university_id"
    t.text     "description"
    t.text     "eligibility"
    t.integer  "fee"
    t.string   "duration"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deducations", :force => true do |t|
    t.date     "transaction_date"
    t.string   "transaction_id"
    t.string   "application_number"
    t.integer  "university_id"
    t.integer  "course_id"
    t.string   "name"
    t.string   "father_name"
    t.string   "gender"
    t.string   "marital_status"
    t.string   "religion"
    t.boolean  "ph_status",          :default => false
    t.boolean  "bpl",                :default => false
    t.date     "dob"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.string   "place"
    t.integer  "pin"
    t.integer  "state_id"
    t.integer  "district_id"
    t.string   "mobile"
    t.string   "email"
    t.string   "qualification1"
    t.string   "year1"
    t.string   "percentage1"
    t.string   "board1"
    t.string   "qualification2"
    t.string   "year2"
    t.string   "percentage2"
    t.string   "board2"
    t.string   "qualification3"
    t.string   "year3"
    t.string   "percentage3"
    t.string   "board3"
    t.string   "qualification4"
    t.string   "year4"
    t.string   "percentage4"
    t.string   "board4"
    t.string   "technical_skill"
    t.boolean  "employee_status",    :default => false
    t.text     "remarks"
    t.integer  "user_id"
    t.boolean  "status",             :default => false
    t.integer  "amount"
    t.string   "payment_mode"
    t.string   "bank_name"
    t.string   "branch"
    t.string   "instrument_number"
    t.date     "instrument_date"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "suspense_status",    :default => false
  end

  create_table "deposits", :force => true do |t|
    t.integer  "user_id"
    t.string   "transaction_number"
    t.date     "transaction_date"
    t.string   "transaction_mode"
    t.string   "transaction_type"
    t.string   "reference_number"
    t.string   "bank_name"
    t.string   "branch"
    t.integer  "instrument_number"
    t.date     "instrument_date"
    t.float    "amount"
    t.boolean  "status",             :default => false
    t.boolean  "fundstatus",         :default => false
    t.text     "remarks"
    t.string   "proof_file_name"
    t.string   "proof_content_type"
    t.integer  "proof_file_size"
    t.datetime "proof_updated_at"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "suspense_status",    :default => false
  end

  create_table "districts", :force => true do |t|
    t.integer  "state_id"
    t.integer  "division_id"
    t.string   "name"
    t.text     "description"
    t.text     "short_code"
    t.boolean  "vle_check"
    t.integer  "dis_number",  :default => 0
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "divisions", :force => true do |t|
    t.integer  "state_id"
    t.string   "name"
    t.text     "description"
    t.string   "short_code"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "downloads", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "upload_file_name"
    t.string   "upload_content_type"
    t.integer  "upload_file_size"
    t.datetime "upload_updated_at"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurances", :force => true do |t|
    t.string   "transaction_id"
    t.date     "transaction_date"
    t.string   "policy_type"
    t.integer  "policyname_id"
    t.string   "application_number"
    t.integer  "sum_assured"
    t.integer  "annual_premium"
    t.integer  "modal_premium"
    t.string   "payment_frequency"
    t.integer  "collected_premium"
    t.string   "payment_mode"
    t.string   "bank_name"
    t.string   "branch_name"
    t.integer  "instrument_number"
    t.date     "instrument_date"
    t.integer  "age"
    t.integer  "term"
    t.string   "policy_holder"
    t.string   "insured_holder"
    t.string   "gender"
    t.date     "dob"
    t.integer  "state_id"
    t.integer  "district_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "place"
    t.integer  "pin"
    t.string   "mobile"
    t.string   "telephone"
    t.date     "physical_date"
    t.string   "courier"
    t.string   "awb_number"
    t.string   "policy_number"
    t.text     "remarks"
    t.boolean  "status",             :default => false
    t.integer  "user_id"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "suspense_status",    :default => false
  end

  create_table "pancards", :force => true do |t|
    t.string   "panapplication_number"
    t.date     "transaction_date"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "dob"
    t.string   "gender"
    t.string   "father_name"
    t.text     "address1"
    t.text     "address2"
    t.text     "address3"
    t.string   "place"
    t.integer  "district_id"
    t.integer  "state_id"
    t.integer  "pincode"
    t.text     "mobile"
    t.text     "telephone"
    t.float    "amount"
    t.string   "payment_mode"
    t.string   "instrument_number"
    t.date     "instrument_date"
    t.string   "bank_name"
    t.string   "branch"
    t.string   "idproof_file_name"
    t.string   "idproof_content_type"
    t.integer  "idproof_file_size"
    t.datetime "idproof_updated_at"
    t.string   "addressproof_file_name"
    t.string   "addressproof_content_type"
    t.integer  "addressproof_file_size"
    t.datetime "addressproof_updated_at"
    t.string   "dobproof_file_name"
    t.string   "dobproof_content_type"
    t.integer  "dobproof_file_size"
    t.datetime "dobproof_updated_at"
    t.text     "remarks"
    t.boolean  "status",                    :default => false
    t.integer  "user_id"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "suspense_status",           :default => false
  end

  create_table "policynames", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "min_age"
    t.integer  "max_age"
    t.float    "min_amount"
    t.float    "max_amount"
    t.integer  "min_term"
    t.integer  "max_term"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "shortmessages", :force => true do |t|
    t.integer  "user_id"
    t.string   "transaction_id"
    t.string   "service"
    t.text     "message"
    t.string   "mobile"
    t.string   "msgid"
    t.string   "status"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "short_code"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", :force => true do |t|
    t.string   "ticket_number"
    t.date     "open_date"
    t.integer  "user_id"
    t.string   "ticket_type"
    t.text     "ticket_description"
    t.text     "problem_solution"
    t.date     "close_date"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.boolean  "status",             :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transactions", :force => true do |t|
    t.integer  "user_id"
    t.string   "transaction_number"
    t.date     "transaction_date"
    t.string   "service"
    t.string   "transaction_mode"
    t.float    "amount"
    t.boolean  "status",             :default => false
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "universities", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.integer  "modified_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "state_id"
    t.integer  "division_id"
    t.integer  "district_id"
    t.integer  "block_id"
    t.string   "panchayat"
    t.string   "csc_code"
    t.string   "vle_code"
    t.float    "available_funds",           :default => 0.0
    t.string   "payment_mode"
    t.string   "bankname"
    t.string   "branch"
    t.integer  "instrument_number"
    t.date     "instrument_date"
    t.text     "remarks"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "idproof_file_name"
    t.string   "idproof_content_type"
    t.integer  "idproof_file_size"
    t.datetime "idproof_updated_at"
    t.string   "addressproof_file_name"
    t.string   "addressproof_content_type"
    t.integer  "addressproof_file_size"
    t.datetime "addressproof_updated_at"
    t.string   "agreement1_file_name"
    t.string   "agreement1_content_type"
    t.integer  "agreement1_file_size"
    t.datetime "agreement1_updated_at"
    t.string   "agreement2_file_name"
    t.string   "agreement2_content_type"
    t.integer  "agreement2_file_size"
    t.datetime "agreement2_updated_at"
    t.string   "agreement3_file_name"
    t.string   "agreement3_content_type"
    t.integer  "agreement3_file_size"
    t.datetime "agreement3_updated_at"
    t.string   "agreement4_file_name"
    t.string   "agreement4_content_type"
    t.integer  "agreement4_file_size"
    t.datetime "agreement4_updated_at"
    t.string   "agreement5_file_name"
    t.string   "agreement5_content_type"
    t.integer  "agreement5_file_size"
    t.datetime "agreement5_updated_at"
    t.string   "agreement6_file_name"
    t.string   "agreement6_content_type"
    t.integer  "agreement6_file_size"
    t.datetime "agreement6_updated_at"
    t.string   "agreement7_file_name"
    t.string   "agreement7_content_type"
    t.integer  "agreement7_file_size"
    t.datetime "agreement7_updated_at"
    t.string   "agreement8_file_name"
    t.string   "agreement8_content_type"
    t.integer  "agreement8_file_size"
    t.datetime "agreement8_updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "name"
    t.string   "father_name"
    t.string   "gender"
    t.date     "dob"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.string   "place"
    t.string   "pin"
    t.string   "mobile_number"
    t.string   "phone_number"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token",                               :null => false
    t.string   "perishable_token",    :default => "",             :null => false
    t.integer  "login_count",         :default => 0,              :null => false
    t.string   "status",              :default => "Not Approved"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "last_request_at"
    t.datetime "last_login_at"
    t.datetime "current_login_at"
    t.string   "last_login_ip"
    t.string   "current_login_ip"
    t.boolean  "approved",            :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
