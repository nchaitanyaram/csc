####Created: Chaitanya #############
########### Users ##################
session = User.create do |u|
  u.name = "Super Admin"
  u.email = 'superadmin@sarkcsc.in'
  u.password = u.password_confirmation = 'admin123'
  u.address1= 'SARK Systems'
  u.mobile_number=9885976490
  u.phone_number=8662524890
  u.pin = 800001
  u.approved = 1
  u.created_by = 1
end
session.save
session = User.create do |u|
  u.name = "Admin"
  u.email = 'admin@sarkcsc.in'
  u.password = u.password_confirmation = 'admin123'
  u.address1='SARK Systems'
  u.mobile_number=9885976490
  u.phone_number=8662524890
  u.pin = 800001
  u.approved = 1
  u.created_by = 1
end
session.save
session = User.create do |u|
  u.name = "Manager"
  u.email = 'manager@sarkcsc.in'
  u.password = u.password_confirmation = 'admin123'
  u.address1='SARK Systems'
  u.mobile_number=9885976490
  u.phone_number=8662524890
  u.pin = 800001
  u.approved = 1
  u.created_by = 1
end
session.save
########## Roles
Role.create(:name=>'super_admin',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'admin',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'manager',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'vle',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'csp',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'district_manager',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
Role.create(:name=>'district_engineer',:created_by=>1,:updated_by=>1,:created_at=>Time.now,:updated_at=>Time.now)
########## Assigmments ###############
Assignment.create(:user_id=>1,:role_id=>1,:created_at=>Time.now,:updated_at=>Time.now)
Assignment.create(:user_id=>2,:role_id=>2,:created_at=>Time.now,:updated_at=>Time.now)
Assignment.create(:user_id=>3,:role_id=>3,:created_at=>Time.now,:updated_at=>Time.now)
##############States###############
State.create(:name=>'Bihar',:description=>'Bihar',:created_by=>1,:short_code=>"BR",:created_at=>Time.now)
#############Divisions#########################
Division.create(:name=>'Bhagalpur',:state_id=>1,:created_by=>1,:short_code=>"BH",:created_at=>Time.now)
Division.create(:name=>'Darbhanga',:state_id=>1,:created_by=>1,:short_code=>"DA",:created_at=>Time.now)
Division.create(:name=>'Kosi',:state_id=>1,:short_code=>"KO",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Magadh',:state_id=>1,:short_code=>"MA",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Munger',:state_id=>1,:short_code=>"MU",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Patna',:state_id=>1,:short_code=>"PA",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Purnia',:state_id=>1,:short_code=>"PU",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Saran',:state_id=>1,:short_code=>"SA",:created_by=>1,:created_at=>Time.now)
Division.create(:name=>'Tirhut',:state_id=>1,:short_code=>"TI",:created_by=>1,:created_at=>Time.now)
#============Districts==================#
District.create(:name=>'Banka',:division_id=>1,:state_id=>1,:short_code=>"BAN",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Bhagalpur',:division_id=>1,:state_id=>1,:short_code=>"BHA",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Begusarai',:division_id=>2,:state_id=>1,:short_code=>"BEG",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Darbhanga',:division_id=>2,:state_id=>1,:short_code=>"DAR",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Madhubani',:division_id=>2,:state_id=>1,:short_code=>"MAH",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Samastipur',:division_id=>2,:state_id=>1,:short_code=>"SAM",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Madhepura',:division_id=>3,:state_id=>1,:short_code=>"MAD",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Saharsa',:division_id=>3,:state_id=>1,:short_code=>"SAH",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Supaul',:division_id=>3,:state_id=>1,:short_code=>"SUP",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Arwal',:division_id=>4,:state_id=>1,:short_code=>"ARW",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Aurangabad',:division_id=>4,:state_id=>1,:short_code=>"AUR",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Gaya',:division_id=>4,:state_id=>1,:short_code=>"GAY",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Jehanabad',:division_id=>4,:state_id=>1,:short_code=>"JEH",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Nawada',:division_id=>4,:state_id=>1,:short_code=>"NAW",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Jamui',:division_id=>5,:state_id=>1,:short_code=>"JAM",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Khagaria',:division_id=>5,:state_id=>1,:short_code=>"KHA",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Munger',:division_id=>5,:state_id=>1,:short_code=>"MUN",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Lakhisarai',:division_id=>5,:state_id=>1,:short_code=>"LAK",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Sheikhpura',:division_id=>5,:state_id=>1,:short_code=>"SHE",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Bhojpur',:division_id=>6,:state_id=>1,:short_code=>"BHO",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Buxar',:division_id=>6,:state_id=>1,:short_code=>"BUX",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Kaimur',:division_id=>6,:state_id=>1,:short_code=>"KAI",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Patna',:division_id=>6,:state_id=>1,:short_code=>"PAT",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Rohtas',:division_id=>6,:state_id=>1,:short_code=>"ROH",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Nalanda',:division_id=>6,:state_id=>1,:short_code=>"NAL",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Araria',:division_id=>7,:state_id=>1,:short_code=>"ARA",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Katihar',:division_id=>7,:state_id=>1,:short_code=>"KAT",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Kishanganj',:division_id=>7,:state_id=>1,:short_code=>"KIS",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Purnia',:division_id=>7,:state_id=>1,:short_code=>"PUR",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Gopalganj',:division_id=>8,:state_id=>1,:short_code=>"GOP",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Chapra',:division_id=>8,:state_id=>1,:short_code=>"CHA",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Siwan',:division_id=>8,:state_id=>1,:short_code=>"SIW",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'East Champaran',:division_id=>9,:state_id=>1,:short_code=>"ECH",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Muzaffarpur',:division_id=>9,:state_id=>1,:short_code=>"MUZ",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Sheohar',:division_id=>9,:state_id=>1,:short_code=>"SHO",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Sitamarhi',:division_id=>9,:state_id=>1,:short_code=>"SIT",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'Vaishali',:division_id=>9,:state_id=>1,:short_code=>"VAI",:vle_check=>0,:dis_number=>0,:created_by=>1,:created_at=>Time.now)
District.create(:name=>'West Champaran',:division_id=>9,:state_id=>1,:short_code=>"WCH",:vle_check=>0,:dis_number=>0, :created_by=>1,:created_at=>Time.now)
#============Blocks========================#
Block.create(:name=>'Manshi',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Isuapur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Garkha',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Tariya',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Marhowrah',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Baniapur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Mashrak',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Ekma',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sonpur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Dighwara',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Chapra Sadar',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Amnour',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Dariyapur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Jalalpur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Parsa',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Panapur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Nagra',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Makar',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Rivilganj',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Lakhadpur',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sadar',:state_id=>1,:division_id=>8,:district_id=>31,:description=>'Enter Description',:created_by=>1,:updated_by=>1)

Block.create(:name=>'Basantpur',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Supaul',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Triveniganj',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Raghopur',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kishanpur',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Marona',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Chhatapur',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Pipra',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Nirmali',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Pratapgunj',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sonagarh Bhaptiyahi',:state_id=>1,:division_id=>3,:district_id=>9,:description=>'Enter Description',:created_by=>1,:updated_by=>1)

Block.create(:name=>'Alam Nagra',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Puraini',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Ghiladh',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Murliganj',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Madhepura',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kumarkhand',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Gamharia',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Gwalpara',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Udakishunganj',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Bihariganj',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Shankarpur',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Chousa',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Singheshwaar',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Ghailar',:state_id=>1,:division_id=>3,:district_id=>7,:description=>'Enter Description',:created_by=>1,:updated_by=>1)

Block.create(:name=>'Vehkagown',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Phulwaria',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kuchai Kote',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Hathua',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Thawe',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Gopalganj',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kateya',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Bhore',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Panchadevri',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Manjha',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Baruli',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Balkunthpur',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sidhwalia',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Uchakagaon',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Vijaypur',:state_id=>1,:division_id=>8,:district_id=>30,:description=>'Enter Description',:created_by=>1,:updated_by=>1)

Block.create(:name=>'Goreakothi',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Barharia',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Bhagwanpur Hat',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Guthani',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Siwan Sadar',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Hussainganj',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Darauli',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Ander',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Pachrukhi',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Basantpur',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Siswan',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Maharajganj',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Mairwa',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Hasanpura',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Darauda',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Lakdinvi Ganj',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Nautan',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Zeradai',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sultanpur',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Raghunathpur',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Lakhnaura',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Duraundha',:state_id=>1,:division_id=>8,:district_id=>32,:description=>'Enter Description',:created_by=>1,:updated_by=>1)

Block.create(:name=>'Sour Bazar',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Nauhatta',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Patarghat',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kahra',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sattar Kattiaya',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Mahisi',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Sonbarsa',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Salkhua',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Simri Bakhtiarupu',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Banma Itahri',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
Block.create(:name=>'Kahra Nagar Panchayat',:state_id=>1,:division_id=>3,:district_id=>8,:description=>'Enter Description',:created_by=>1,:updated_by=>1)
#============Policy Name#=====================
Policyname.create(:name=>'DLF Pramerica Dhan Suraksha',:description=>'DLF Pramerica Dhan Suraksha',:created_by=>1,:updated_by=>1,:created_at=>Time.now)
