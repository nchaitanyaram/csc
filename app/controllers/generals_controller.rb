class GeneralsController < ApplicationController
  layout "application"
  before_filter :require_user
  def index
    @users = User.admin_users if has_role?(:admin) or has_role?(:manager)
    @users_approved = User.admin_users.where(:approved=>true) if has_role?(:admin)  or has_role?(:manager)
    @vle = User.includes([:roles]).where(:roles=>{:name=>"vle"},:approved=>true).size if has_role?(:admin)  or has_role?(:manager)
    @dlf = User.includes([:roles]).where(:roles=>{:name=>"dlf_agent"},:approved=>true).size if has_role?(:admin)  or has_role?(:manager)
    @dm = User.includes([:roles]).where(:roles=>{:name=>"district_manager"},:approved=>true).size if has_role?(:admin)  or has_role?(:manager)
    @de = User.includes([:roles]).where(:roles=>{:name=>"district_engineer"},:approved=>true).size if has_role?(:admin)    or has_role?(:manager)
    @vle_district = User.includes([:roles]).joins([:user_profile]).where(:user_profile=>{:district_id=>current_user.user_profile.district_id},:roles=>{:name=>"vle"},:approved=>true).size if has_role?(:district_manager) or has_role?(:district_engineer)
    @csp_district = User.includes([:roles]).joins([:user_profile]).where(:user_profile=>{:district_id=>current_user.user_profile.district_id},:roles=>{:name=>"csp"},:approved=>true).size if has_role?(:district_manager) or has_role?(:district_engineer)
    @dm_district = User.includes([:roles]).joins([:user_profile]).where(:user_profile=>{:district_id=>current_user.user_profile.district_id},:roles=>{:name=>"district_manager"},:approved=>true).size if has_role?(:district_manager) or has_role?(:district_engineer)
    @de_district = User.includes([:roles]).joins([:user_profile]).where(:user_profile=>{:district_id=>current_user.user_profile.district_id},:roles=>{:name=>"district_engineer"},:approved=>true).size  if has_role?(:district_manager) or has_role?(:district_engineer)

    @users_district = User.joins([:user_profile]).where(:user_profile=>{:district_id => current_user.user_profile.district_id}) if has_role?(:district_manager) or has_role?(:district_engineer)
    @users_district_approved = User.joins([:user_profile]).where(:user_profile=>{:district_id => current_user.user_profile.district_id},:approved=>true) if has_role?(:district_manager) or has_role?(:district_engineer)


    @insurances = Insurance.all if has_role?(:admin) or has_role?(:manager)
    @insurances_approved = Insurance.where(:status =>  true,:suspense_status => false) if has_role?(:admin)  or has_role?(:manager)
    @insurances_pending = Insurance.where(:status=>false,:suspense_status => false) if has_role?(:admin) or has_role?(:manager)
    @insurances_suspense = Insurance.where(:suspense_status => true)
    @insurances_user = Insurance.where(:user_id=>current_user.id) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances_user_approved = Insurance.where(:user_id=>current_user.id,:status =>  true,:suspense_status => false) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances_user_pending = Insurance.where(:user_id=>current_user.id,:status =>  true,:suspense_status => true) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances_user_suspense = Insurance.where(:user_id=>current_user.id,:suspense_status =>  true) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances_district = Insurance.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}) if has_role?(:district_manager) or has_role?(:district_engineer)
    @insurances_district_approved = Insurance.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>true) if has_role?(:district_manager) or has_role?(:district_engineer)


    @pans = Pancard.all if has_role?(:admin) or has_role?(:manager)
    @pans_user = Pancard.where(:user_id => current_user.id) if has_role?(:vle)
    @pans_approved = Pancard.where(:status =>  true) if has_role?(:admin)  or has_role?(:manager)
    @pans_user_approved = Pancard.where(:status =>  true,:user_id => current_user.id) if has_role?(:vle)
    @pancards_district = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}) if has_role?(:district_manager) or has_role?(:district_engineer)
    @pancards_district_approved = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>true) if has_role?(:district_manager) or has_role?(:district_engineer)

    @deposits = Deposit.all if has_role?(:admin)  or has_role?(:manager)
    @deposits_user = Deposit.where(:user_id => current_user.id) if has_role?(:vle) or has_role?(:dlf_agent)
    @deposits_approved = Deposit.where(:status =>  true) if has_role?(:admin)  or has_role?(:manager)
    @deposits_user_approved = Deposit.where(:status =>  true,:user_id => current_user.id) if has_role?(:vle) or has_role?(:dlf_agent)


    @tickets = Ticket.all if has_role?(:admin) or has_role?(:state_engineer)
    @tickets_closed = Ticket.where(:status=>true) if has_role?(:admin) or has_role?(:state_engineer)
    @tickets_normal = Ticket.where(:status=>false,:ticket_type => "Normal") if has_role?(:admin)  or has_role?(:state_engineer)
    @tickets_urgent = Ticket.where(:status=>false,:ticket_type => "Urgent") if has_role?(:admin)  or has_role?(:state_engineer)
    @tickets_district = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}) if has_role?(:district_manager) or has_role?(:district_engineer)
    @tickets_district_closed = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>true) if has_role?(:district_manager) or has_role?(:district_engineer)
    @tickets_district_normal = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>false,:ticket_type=>"Normal") if has_role?(:district_manager) or has_role?(:district_engineer)
    @tickets_district_urgent = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>false,:ticket_type=>"Urgent") if has_role?(:district_manager) or has_role?(:district_engineer)
    @tickets_user = Ticket.where(:created_by=>current_user.id) if has_role?(:vle)
    @tickets_user_closed = Ticket.where(:created_by=>current_user.id,:status => true) if has_role?(:vle)
    @tickets_user_normal = Ticket.where(:created_by=>current_user.id,:status => false,:ticket_type => "Normal") if has_role?(:vle)
    @tickets_user_urgent = Ticket.where(:created_by=>current_user.id,:status => false,:ticket_type => "Urgent") if has_role?(:vle)

    @apps = Deducation.all if has_role?(:admin) or has_role?(:manager)
    @apps_user = Deducation.where(:user_id => current_user.id) if has_role?(:vle)
    @apps_approved = Deducation.where(:status =>  true) if has_role?(:admin)  or has_role?(:manager)
    @apps_user_approved = Deducation.where(:status =>  true,:user_id => current_user.id) if has_role?(:vle)
#    @apps_district = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}) if has_role?(:district_manager) or has_role?(:district_engineer)
#    @apps_district_approved = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:status=>true) if has_role?(:district_manager) or has_role?(:district_engineer)

  end
end
