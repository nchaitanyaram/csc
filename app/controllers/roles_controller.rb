#Author: Chaitanya
#Controller: Roles
#Date: 26 May 2011
#===================================
class RolesController < ApplicationController
  layout "admin"
  before_filter :require_user,:recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction
  def index
    @roles = Role.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @roles }
      format.js
    end
  end

  def show
    @role = Role.find(params[:id])
    render :layout => "application"

  end

  def new
    @role = Role.new
    render :layout => "application"
  end

  def edit
    @role = Role.find(params[:id])
    render :layout => "application"
  end

  def create
    @role = Role.new(params[:role])
    @role.created_by = @created_by

    respond_to do |format|
      if @role.save
        format.html { redirect_to(@role, :notice => 'Role was successfully created.') }
        format.xml  { render :xml => @role, :status => :created, :location => @role }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @role = Role.find(params[:id])
    @role.updated_by = @created_by
    
    respond_to do |format|
      if @role.update_attributes(params[:role])
        format.html { redirect_to(@role, :notice => 'Role was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @role = Role.find(params[:id])
    @role.destroy

    respond_to do |format|
      format.html { redirect_to(roles_url) }
      format.xml  { head :ok }
    end
  end
  private

  def recent_items
    @recent_roles = Role.order('created_at DESC').limit(5)
  end

  def sort_column
    Role.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
