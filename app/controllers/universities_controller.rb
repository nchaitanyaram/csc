#Author: Chaitanya
################################################################
class UniversitiesController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @universities = University.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @universities }
      format.js
    end
  end

  def show
    @university = University.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @university }
    end
  end

  def new
    @university = University.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @university }
    end
  end

  def edit
    @university = University.find(params[:id])
  end

  def create
    @university = University.new(params[:university])

    respond_to do |format|
      if @university.save
        format.html { redirect_to(@university, :notice => 'University was successfully created.') }
        format.xml  { render :xml => @university, :status => :created, :location => @university }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @university.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @university = University.find(params[:id])

    respond_to do |format|
      if @university.update_attributes(params[:university])
        format.html { redirect_to(@university, :notice => 'University was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @university.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @university = University.find(params[:id])
    @university.destroy

    respond_to do |format|
      format.html { redirect_to(universities_url) }
      format.xml  { head :ok }
    end
  end
  ########################################  Private Methods #######################################################
  private
  def recent_items
    @recent_universities= University.order('created_at DESC').limit(5) if has_role?(:admin)
  end

  def sort_column
    University.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  ######################################## Private Methods ############################################################

end
