#Author: Chaitanya
##################################################
class DeducationsController < ApplicationController
  layout "admin", :except => :print
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction
######## For SMS Gateway ##############
  require 'net/http'
  require 'uri'
####################################
  def index
    @deducations = Deducation.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin) or has_role?(:manager)
    @deducations = Deducation.where(:created_by=>current_user.id).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deducations }
      format.js
    end
  end

  def show
    @deducation = Deducation.find(params[:id])
    render :layout => "application"
  end

  def new
    @deducation = Deducation.new
    render :layout => "application"
  end

  def edit
    @deducation = Deducation.find(params[:id])
  end

  def create
    @deducation = Deducation.new(params[:deducation])
    @deducation.created_by = current_user.id
    @deducation.transaction_id = 'DEDU/' + current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s
    @deducation.user_id = current_user.id
    @deducation.status = false

    @user_profile_user = UserProfile.find(:first, :conditions=>{:user_id=>current_user.id})
    user_profile_user_funds = @user_profile_user.available_funds

    if user_profile_user_funds <=0
      flash[:error] = "Sorry! You have No Funds to perform Transaction.Please deposit Funds to continue the Transactions"
      redirect_to(deducations_url)
    else
      user_profile_remaining_funds = user_profile_user_funds - params[:deducation][:amount].to_f #transaction_records_sum

      if user_profile_remaining_funds < 0
        flash[:error] = "Sorry! You have Insufficient Funds.Transaction is Cancelled. Please deposit Funds to continue the Transactions"
        redirect_to(deducations_url)
      else
        respond_to do |format|
          if @deducation.save
            @deducation_transaction = Transaction.new(:user_id=>current_user.id, :transaction_number=>@deducation.transaction_id, :transaction_date=>Date.today, :transaction_mode=>'Dr', :service=>"Education", :amount=>params[:deducation][:amount])
            @deducation_transaction.save
            @user_profile_user.available_funds = user_profile_remaining_funds
            @user_profile_user.save

            sms_status # sms

            format.html { redirect_to(@deducation, :notice => 'Application was successfully created.') }
            format.xml { render :xml => @deducation, :status => :created, :location => @deducation }
          else
            format.html { render :action => "new" }
            format.xml { render :xml => @deducation.errors, :status => :unprocessable_entity }
          end
        end
      end
    end
  end

  def update
    @deducation = Deducation.find(params[:id])

    respond_to do |format|
      if @deducation.update_attributes(params[:deducation])
        format.html { redirect_to(@deducation, :notice => 'Application was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @deducation.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @deducation = Deducation.find(params[:id])
    @deducation.destroy

    respond_to do |format|
      format.html { redirect_to(deducations_url) }
      format.xml { head :ok }
    end
  end

  def payment_mode
    respond_to do |format|
      format.js
    end
  end

  def get_amount
    @course = Course.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def update_status
    deducation = Deducation.find(params[:id])
    deducation.update_attribute('status', deducation.status ? false : true)
    flash[:success] = "Application Status has been updated."
    respond_to do |format|
      format.html { redirect_to(:controller=>'deducations', :action=>'index') }
    end
  end

  def load_courses
    @load_courses = Course.find_all_by_university_id(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def sms_status
    mobile = @deducation.mobile
    #name = @pancard.first_name + @pancard.last_name
    #message = "Dear+Customer,+A Premium+of+Rs."+@insurance.collected_premium.to_s+"/-+received+towards+"+@insurance.policyname.name+"+Policy.+Your+Reference+No:+"+@insurance.transaction_id+"."
    message = "Dear Customer, A Fee of Rs." + @deducation.amount.to_s + "/- received towards "+ @deducation.course.name + ". Your Reference No: "+ @deducation.transaction_id + "."
    url = "http://voice.full2ads.co.cc/api/sms.php?uid=6368616974616e7961&pin=4e4ce8b0a500c&sender=SARKCSC&route=1&mobile=#{mobile},9934186169&message=#{message}"
    Net::HTTP.get_print URI.parse(URI.encode(url.strip))

    @deducation_sms = Shortmessage.new(:user_id=>current_user.id, :transaction_id=>@deducation.transaction_id, :service=>'Education', :message=>message, :mobile=>mobile)
    @deducation_sms.save
  end

  def print
    @deducation = Deducation.find(params[:id])
    html = render_to_string :layout => false
    kit = PDFKit.new(html) #,:page_size => 'Legal')
    #kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Application_Print.pdf", :type => 'application/pdf')

  end

##################################################
  private
  def recent_items
    @recent_deducations = Deducation.order('created_at DESC').limit(5) if has_role?(:admin) or has_role?(:manager)
    @recent_deducations = Deducation.where(:created_by=>current_user.id).order('created_at DESC').limit(5) if has_role?(:vle)
  end

  def sort_column
    Deducation.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

end
