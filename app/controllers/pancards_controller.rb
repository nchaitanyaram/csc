#Author:Chaitanya Ram
#=========================================
class PancardsController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction
######## For SMS Gateway ##############
  require 'net/http'
  require 'uri'
####################################
  def index
    @pancards = Pancard.where(:created_by=>current_user.id,:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle)
    @pancards = Pancard.where(:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin) or has_role?(:manager)
    @pancards = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:suspense_status=>false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @pancards }
      format.js
    end
  end

  def show
    @pancard = Pancard.find(params[:id])
    render :layout => "application"
  end

  def new
    @pancard = Pancard.new
    render :layout => "application"
  end

  def edit
    @pancard = Pancard.find(params[:id])
    render :layout => "application"
  end

  def create
    @pancard = Pancard.new(params[:pancard])
    @pancard.created_by = @pancard.user_id = current_user.id
    @pancard.panapplication_number = 'PAN/' + current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s
    @pancard.dob = params[:dob]
    @user_profile_user = UserProfile.find(:first, :conditions=>{:user_id=>current_user.id})
    user_profile_user_funds = @user_profile_user.available_funds

    if user_profile_user_funds <=0
      flash[:error] = "Sorry! You have No Funds to perform Transaction."
      redirect_to(insurances_url)
    else
      user_profile_remaining_funds = user_profile_user_funds - params[:pancard][:amount].to_f #transaction_records_sum

      if user_profile_remaining_funds < 0
        flash[:error] = "Sorry! You have Insufficient Funds.Transaction is Cancelled."
        redirect_to(insurances_url)
      else
        respond_to do |format|
          if @pancard.save
            @pancard_transaction = Transaction.new(:user_id=>current_user.id, :transaction_number=>@pancard.panapplication_number, :transaction_date=>Date.today, :transaction_mode=>'Dr', :service=>"PAN", :amount=>params[:pancard][:amount])
            @pancard_transaction.save
            @user_profile_user.available_funds = user_profile_remaining_funds
            @user_profile_user.save

            sms_status ### Send SMS

            format.html { redirect_to(@pancard, :notice => 'PAN Application was successfully Created.') }
            format.xml { render :xml => @pancard, :status => :created, :location => @pancard }
          else
            format.html { render :action => "new" }
            format.xml { render :xml => @pancard.errors, :status => :unprocessable_entity }
          end
        end
      end
    end
  end

  def update
    @pancard = Pancard.find(params[:id])

    respond_to do |format|
      if @pancard.update_attributes(params[:pancard])
        format.html { redirect_to(@pancard, :notice => 'PAN Application was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @pancard.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @pancard = Pancard.find(params[:id])
    @pancard.destroy

    respond_to do |format|
      format.html { redirect_to(pancards_url) }
      format.xml { head :ok }
    end
  end

  def update_status
    pancard = Pancard.find(params[:id])
    pancard.update_attribute('status', pancard.status ? false : true)
    flash[:success] = "PAN Application Status has been updated."
    respond_to do |format|
      format.html { redirect_to(:controller=>'pancards', :action=>'index') }
    end
  end

  def payment_mode
    respond_to do |format|
      format.js
    end
  end

  def print
    @pancard = Pancard.find(params[:id])

    #qrcode content
    puts @qrcode = @pancard.panapplication_number + ',' + @pancard.transaction_date.strftime('%d-%m-%y') + ',' + @pancard.amount.to_s+ ',' + UserProfile.find_by_user_id(@pancard.user_id).vle_code + ',' + UserProfile.find_by_user_id(@pancard.user_id).panchayat + '/' + UserProfile.find_by_user_id(@pancard.user_id).district.name + ',' + UserProfile.find_by_user_id(@pancard.user_id).district_id.to_s + UserProfile.find_by_user_id(@pancard.user_id).state_id.to_s + @pancard.user_id.to_s + UserProfile.find_by_user_id(@pancard.user_id).id.to_s

    #PDF Render
    html = render_to_string :layout => false
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css'
    send_data(kit.to_pdf, :filename => "PAN Receipt-#{@pancard.panapplication_number}.pdf", :type => 'application/pdf')
  end

  def sms_status
    mobile = @pancard.mobile
    message = "Dear Customer,Received amount of Rs."+@pancard.amount.to_s+"/- towards PAN Application.Your Reference No: "+@pancard.panapplication_number+"."
    url = "http://voice.full2ads.co.cc/api/sms.php?uid=6368616974616e7961&pin=4e4ce8b0a500c&sender=SARKCSC&route=1&mobile=#{mobile},9934186169&message=#{message}"
    Net::HTTP.get_print URI.parse(URI.encode(url.strip))

    @pancard_sms = Shortmessage.new(:user_id=>current_user.id, :transaction_id=>@pancard.panapplication_number, :service=>'PAN', :message=>message, :mobile=>mobile)
    @pancard_sms.save
  end

  def suspense
    pancard = Pancard.find(params[:id])
    pancard.update_attribute('suspense_status', pancard.suspense_status ? false : true)
    if pancard.suspense_status?
      flash[:success] = "PAN Card Transaction is Suspended."
      respond_to do |format|
        format.html { redirect_to(:controller=>'pancards', :action=>'index') }
      end
    else
      flash[:success] = "PAN Card Transaction is Reverted successfully"
      respond_to do |format|
        format.html { redirect_to(:controller=>'pancards', :action=>'suspense_transactions') }
      end
    end
  end

  def suspense_transactions
    @pancards = Pancard.where(:created_by=>current_user.id,:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle)
    @pancards = Pancard.where(:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin) or has_role?(:manager)
    @pancards = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:suspense_status=>true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @pancards }
    end
  end

########################################  Private Methods #######################################################
  private
  def recent_items
    @recent_pancards = Pancard.where(:created_by=>current_user.id).order('created_at DESC').limit(5) if has_role?(:vle)
    @recent_pancards = Pancard.order('created_at DESC').limit(5) if has_role?(:admin)
    @recent_pancards = Pancard.order('created_at DESC').limit(5) if has_role?(:manager)
    @recent_pancards = Pancard.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).order('pancards.created_at DESC').limit(5) if has_role?(:district_manager) or has_role?(:district_engineer)
  end

  def sort_column
    Pancard.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
######################################### Private Methods ############################################################
end