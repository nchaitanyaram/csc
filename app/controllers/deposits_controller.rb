#Created : Chaitanya Ram N
#======================================
class DepositsController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
=begin
    @deposits = Deposit.where(:created_by=>current_user.id).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle)
    @deposits = Deposit.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @deposits = Deposit.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)
=end

    @deposits = Deposit.where(:user_id=>current_user.id,:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle) or has_role?(:dlf_agent)
    @deposits = Deposit.where(:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @deposits = Deposit.where(:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deposits }
      format.js
    end
  end

  def show
    @deposit = Deposit.find(params[:id])
    render :layout => "application"
  end

  def new
    @deposit = Deposit.new
    render :layout => "application"
  end

  def edit
    @deposit = Deposit.find(params[:id])
  end

  def create
    @deposit = Deposit.new(params[:deposit])
    @deposit.created_by = current_user.id
    @deposit.transaction_number = 'FUND/' + current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s
    @deposit.user_id = current_user.id
    @deposit.fundstatus = false

    respond_to do |format|
      if @deposit.save
        format.html { redirect_to(@deposit, :notice => 'Fund Transaction was successfully created.') }
        format.xml { render :xml => @deposit, :status => :created, :location => @deposit }
        sms_status
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @deposit.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @deposit = Deposit.find(params[:id])

    respond_to do |format|
      if @deposit.update_attributes(params[:deposit])
        format.html { redirect_to(@deposit, :notice => 'Fund Transaction was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @deposit.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @deposit = Deposit.find(params[:id])
    @deposit.destroy

    respond_to do |format|
      format.html { redirect_to(deposits_url) }
      format.xml { head :ok }
    end
  end

  def transaction_mode
    respond_to do |format|
      format.js
    end
  end

  def update_status
    deposit = Deposit.find(params[:id])
    deposit.update_attribute('status', deposit.status ? false : true)
    flash[:success] = "Transaction Status has been updated."
    respond_to do |format|
      format.html { redirect_to(:controller=>'deposits', :action=>'index') }
    end
  end

  def update_funds
    @deposit = Deposit.find(params[:id])
    @deposit_amount = @deposit.amount

    @get_user = UserProfile.find_by_user_id(@deposit.user_id)
    old_balance = @get_user.available_funds

    new_balance = (old_balance + @deposit_amount).to_f

    @get_user.available_funds = new_balance
    @get_user.save
    @deposit.fundstatus = true
    @deposit.save

    @deposit_transaction = Transaction.new(:user_id=>@deposit.user_id, :transaction_number=>@deposit.transaction_number, :transaction_date=>@deposit.transaction_date, :transaction_mode=>'Cr', :service=>"Deposit", :amount=>@deposit.amount)
    @deposit_transaction.save

    sms_deposit_status ## sms after fund deposited to VLE account

    respond_to do |format|
      format.html { redirect_to(deposits_url) }
      format.xml { head :ok }
    end
  end

  def upload_funds
    @users = User.admin_users.where(:approved => true).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    render :layout => "application"
  end

  def upload
    @user = User.find(params[:id])
    @deposit = Deposit.new
    render :layout => nil
  end

  def fund_save
    @deposit = Deposit.new(params[:deposit])
    @deposit.user_id=params[:deposit][:user_id]
    @deposit.amount= params[:deposit][:amount]
    @deposit.transaction_date= Date.today
    @deposit.created_by = current_user.id
    @deposit.transaction_number = 'FUND/' + params[:deposit][:user_id].to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s
#    @deposit.transaction_mode= 'Cash'
    @deposit.fundstatus = false
    @deposit.save
    redirect_to :back

  end

  def sms_status
    mobile = User.find_by_id(current_user.id).mobile_number
    message = "Dear VLE,Amount of Rs."+ @deposit.amount.to_s+"/- towards Deposit.Your funds are subject to Realization. Your Reference No: "+@deposit.transaction_number+"."
    url = "http://voice.full2ads.co.cc/api/sms.php?uid=6368616974616e7961&pin=4e4ce8b0a500c&sender=SARKCSC&route=1&mobile=#{mobile},9934186169&message=#{message}"
    Net::HTTP.get_print URI.parse(URI.encode(url.strip))

    @fund_sms = Shortmessage.new(:user_id=>current_user.id, :transaction_id=>@deposit.transaction_number, :service=>'FUNDS Deposit', :message=>message, :mobile=>mobile)
    @fund_sms.save
  end

  def sms_deposit_status
    mobile = User.find_by_id(@deposit.user_id).mobile_number
    message = "Dear VLE,Amount of Rs."+ @deposit_amount.to_s+"/- Deposited to Your account for Reference No: "+@deposit.transaction_number+"."
    url = "http://voice.full2ads.co.cc/api/sms.php?uid=6368616974616e7961&pin=4e4ce8b0a500c&sender=SARKCSC&route=1&mobile=#{mobile},9934186169&message=#{message}"
    Net::HTTP.get_print URI.parse(URI.encode(url.strip))

    @fund_sms = Shortmessage.new(:user_id=>@deposit.user_id, :transaction_id=>@deposit.transaction_number, :service=>'FUNDS Approve', :message=>message, :mobile=>mobile)
    @fund_sms.save
  end

  def suspense
    deposit = Deposit.find(params[:id])
    deposit.update_attribute('suspense_status', deposit.suspense_status ? false : true)
    if deposit.suspense_status?
    flash[:success] = "Fund Transaction is Suspended."
    respond_to do |format|
      format.html { redirect_to(:controller=>'deposits', :action=>'index') }
    end
    else
    flash[:success] = "Transaction is Reverted successfully"
    respond_to do |format|
      format.html { redirect_to(:controller=>'deposits', :action=>'suspense_transactions') }
    end
    end

  end
  def suspense_transactions
    @deposits = Deposit.where(:user_id=>current_user.id,:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle) or has_role?(:dlf_agent)
    @deposits = Deposit.where(:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @deposits = Deposit.where(:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deposits }
      format.js
    end

  end

################################################## Private Methods ################
  private
  def recent_items
    @recent_deposits = Deposit.where(:user_id=>current_user.id).order('created_at DESC').limit(5) if has_role?(:vle) or has_role?(:dlf_agent)
    @recent_deposits = Deposit.order('created_at DESC').limit(5) if has_role?(:admin)
    @recent_deposits = Deposit.order('created_at DESC').limit(5) if has_role?(:manager)

  end

  def sort_column
    Deposit.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
#####################################################################################
end
