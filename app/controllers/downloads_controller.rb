#Author: Chaitanya
#==============================================
class DownloadsController < ApplicationController
  layout "application"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @downloads = Download.order(sort_column + ' ' + sort_direction).paginate(:page =>page,:per_page=>per_page )

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @downloads }
      format.js
    end
  end

  def show
    @download = Download.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @download }
    end
  end

  def new
    @download = Download.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml { render :xml => @download }
    end
  end

  def edit
    @download = Download.find(params[:id])
  end

  def create
    @download = Download.new(params[:download])
    @download.created_by = current_user.id

    respond_to do |format|
      if @download.save
        format.html { redirect_to(@download, :notice => 'File Uploaded Successfully.') }
        format.xml { render :xml => @download, :status => :created, :location => @download }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @download.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @download = Download.find(params[:id])

    respond_to do |format|
      if @download.update_attributes(params[:download])
        format.html { redirect_to(@download, :notice => 'Download was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @download.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @download = Download.find(params[:id])
    @download.destroy

    respond_to do |format|
      format.html { redirect_to(downloads_url) }
      format.xml { head :ok }
    end
  end

#########################################################################################################
  private

  def recent_items
    @recent_downloads = Download.order('created_at DESC').limit(5)
  end

  def sort_column
    Download.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
#########################################################################################################
end
