#Author: Chaitanya Ram N
#============================================
require "csv"
class ReportsController < ApplicationController
  layout "application"
  before_filter :require_user
  filter_access_to :all
############################# USER REPORTS #####################################################
  def users
    @users = User.admin_users.paginate(:page =>page, :per_page=>per_page)
  end

  def user_report
    @users = User.admin_users.order('users.name ASC')
    outfile = "User Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Name", "Email", "Mobile", "Division", "District", "Block", "Panchayat", "VLE ID", "CSC Code"]
      @users.each do |user|
        csv << [user.approved? ? 'Approved' : 'Pending', user.name, user.email, user.mobile_number, user.user_profile.division.name, user.user_profile.district.name, user.user_profile.block_id? ? user.user_profile.block.name : 'None', user.user_profile.panchayat, user.user_profile.vle_code, user.user_profile.csc_code]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def user_activity
    @users = User.admin_users.order('users.name ASC')
    outfile = "User Activity Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Name", "Email", "Mobile", "Division", "District", "Block", "Panchayat", "VLE ID", "Login Count", "Last Login At"]
      @users.each do |user|
        csv << [user.approved? ? 'Approved' : 'Pending', user.name, user.email, user.mobile_number, user.user_profile.division.name, user.user_profile.district.name, user.user_profile.block_id? ? user.user_profile.block.name : 'None', user.user_profile.panchayat, user.user_profile.vle_code, user.login_count, user.last_login_at? ? user.last_login_at.strftime("%d-%m-%Y at %H:%M") : '']
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def user_district_export
    @users = User.joins([:user_profile]).where(:user_profile=>{:district_id => params[:district_id]}).order("users.name ASC")
    outfile = "User District Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Name", "Email", "Mobile", "Division", "District", "Block", "Panchayat", "VLE ID"]
      @users.each do |user|
        csv << [user.approved? ? 'Approved' : 'Pending', user.name, user.email, user.mobile_number, user.user_profile.division.name, user.user_profile.district.name, user.user_profile.block_id? ? user.user_profile.block.name : 'None', user.user_profile.panchayat, user.user_profile.vle_code]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def user_log_report
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @users = User.admin_users.paginate(:page =>page, :per_page=>per_page)
    else
      @users = User.admin_users.where(:updated_at => params[:start_date].to_date.beginning_of_day..params[:end_date].to_date.end_of_day).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def user_log_export
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @users = User.admin_users.where(:updated_at => params[:start_date].to_date.beginning_of_day..params[:end_date].to_date.end_of_day).paginate(:page =>page, :per_page=>per_page)

    outfile = "User Log Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Name", "Email", "Mobile", "Division", "District", "Block", "Panchayat", "VLE ID", "Login Date"]
      @users.each do |user|
        csv << [user.name, user.email, user.mobile_number, user.user_profile.division.name, user.user_profile.district.name, user.user_profile.block_id? ? user.user_profile.block.name : 'None', user.user_profile.panchayat, user.user_profile.vle_code, user.updated_at.strftime("%d-%m-%Y")]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"

  end

############################ End USER REPORTS #####################################################
############################ USER ACCOUNT REPORTS #################################################
  def user_accounts
    @users = User.admin_users.paginate(:page =>page, :per_page=>per_page)
#     render :layout => "admin"
  end

  def account_statement
    if has_role?(:admin) or has_role?(:manager)
    @user = User.find(params[:id])
    @transactions = Transaction.order("transaction_date ASC, transaction_mode ASC").where(:user_id => params[:id]).paginate(:page =>page, :per_page=>per_page)
    elsif has_role?(:vle) or has_role?(:dlf_agent)
     @transactions = Transaction.order("transaction_date ASC, transaction_mode ASC").where(:user_id => current_user.id).paginate(:page =>page, :per_page=>per_page)
    else
    end
  end
############################ End USER ACCOUNT REPORT ############################################

############################ INSURANCE REPORTS ###################################################
  def insurances
    @insurances = Insurance.where(:suspense_status => false).order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
  end

  def insurance_eodreport
    @insurances = Insurance.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
  end

  def insurance_eodreport_export
    @insurances = Insurance.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
    outfile = "Insurance EOD Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Policy Type", "Policy Name", "Application No", "Retail ID", "Retail Name", "Retail Location", "District", "State", "SARK Transaction ID", "VLE Code", "Name of Policy Holder", "Name of Insured Person", "Gender", "DOB", "Sum Assured", "Address", "Mobile", "Telephone", "Payment Frequency", "Annual Premium", "Modal Premium", "Collected Premium", "Policy Number", "SARK Remarks", "AWB No", "Date of Dispatch", "Courier"]
      @insurances.each do |insurance|
        if insurance.policy_type == 'New'
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, insurance.application_number, 'SARK Systems India Limited', 'SARK Systems India Limited', 'PATNA', insurance.district.name, insurance.state.name, insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, insurance.policy_holder.titleize, insurance.insured_holder.titleize, insurance.gender, insurance.dob, insurance.sum_assured, insurance.address1, insurance.mobile, insurance.telephone, insurance.payment_frequency, insurance.annual_premium, insurance.modal_premium, insurance.collected_premium, '', insurance.remarks, insurance.awb_number, insurance.physical_date, insurance.courier]
        else
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, '', 'SARK System India Limited', 'SARK Systems India Limited', 'PATNA', '', '', insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, '', '', '', '', '', '', '', '', '', '', '', insurance.collected_premium, insurance.policy_number, insurance.remarks, '', '', '']
        end
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def insurance_daterangereport
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @insurances = Insurance.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
    else
      @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def insurance_daterangexport
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date)
    outfile = "Insurance Date Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Policy Type", "Policy Name", "Application No", "Retail ID", "Retail Name", "Retail Location", "District", "State", "SARK Transaction ID", "VLE Code", "Name of Policy Holder", "Name of Insured Person", "Gender", "DOB", "Sum Assured", "Address", "Mobile", "Telephone", "Payment Frequency", "Annual Premium", "Modal Premium", "Collected Premium", "Policy Number", "SARK Remarks", "AWB No", "Date of Dispatch", "Courier"]
      @insurances.each do |insurance|
        if insurance.policy_type == 'New'
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, insurance.application_number, '12345', 'SARK', 'PATNA', insurance.district.name, insurance.state.name, insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, insurance.policy_holder, insurance.insured_holder, insurance.gender, insurance.dob, insurance.sum_assured, insurance.address1, insurance.mobile, insurance.telephone, insurance.payment_frequency, insurance.annual_premium, insurance.modal_premium, insurance.collected_premium, insurance.application_number, insurance.remarks, insurance.awb_number, insurance.physical_date, insurance.courier]
        else
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, '', 'SARK System India Limited', 'SARK Systems India Limited', 'PATNA', '', '', insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, '', '', '', '', '', '', '', '', '', '', '', insurance.collected_premium, insurance.policy_number, insurance.remarks, '', '', '']
        end
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def insurance_export
    @insurances = Insurance.all
    outfile = "Insurance_Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Policy Type", "Policy Name", "Application No", "Retail ID", "Retail Name", "Retail Location", "District", "State", "SARK Transaction ID", "VLE Code", "Name of Policy Holder", "Name of Insured Person", "Gender", "DOB", "Sum Assured", "Address", "Mobile", "Telephone", "Payment Frequency", "Annual Premium", "Modal Premium", "Collected Premium", "Policy Number", "SARK Remarks", "AWB No", "Date of Dispatch", "Courier"]
      @insurances.each do |insurance|
        if insurance.policy_type == 'New'
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, insurance.application_number, 'SARK Systems India Limited', 'SARK Systems India Limited', 'PATNA', insurance.district.name, insurance.state.name, insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, insurance.policy_holder.titleize, insurance.insured_holder.titleize, insurance.gender, insurance.dob, insurance.sum_assured, insurance.address1, insurance.mobile, insurance.telephone, insurance.payment_frequency, insurance.annual_premium, insurance.modal_premium, insurance.collected_premium, '', insurance.remarks, insurance.awb_number, insurance.physical_date, insurance.courier]
        else
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, '', 'SARK System India Limited', 'SARK Systems India Limited', 'PATNA', '', '', insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, '', '', '', '', '', '', '', '', '', '', '', insurance.collected_premium, insurance.policy_number, insurance.remarks, '', '', '']
        end
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def insurance_eodpayment
    @insurances = Insurance.where(:transaction_date=>Date.today)
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape', :page_size => 'Legal')
#       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Insurance EOD Transactions.pdf", :type => 'application/pdf')

  end

  def insurance_datepayment
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date, :suspense_status => false)

    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape', :page_size => 'Legal')
#       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Insurance EOD Transactions.pdf", :type => 'application/pdf')

  end

  def ins_suspense_transactions
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @insurances = Insurance.where(:suspense_status => true).order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
    else
      @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date, :suspense_status => true).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def insu_suspense_export
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date, :suspense_status => true)
    outfile = "Insurance_suspense_Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Policy Type", "Policy Name", "Application No", "Retail ID", "Retail Name", "Retail Location", "District", "State", "SARK Transaction ID", "VLE Code", "Name of Policy Holder", "Name of Insured Person", "Gender", "DOB", "Sum Assured", "Address", "Mobile", "Telephone", "Payment Frequency", "Annual Premium", "Modal Premium", "Collected Premium", "Policy Number", "SARK Remarks", "AWB No", "Date of Dispatch", "Courier"]
      @insurances.each do |insurance|
        if insurance.policy_type == 'New'
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, insurance.application_number, '12345', 'SARK', 'PATNA', insurance.district.name, insurance.state.name, insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, insurance.policy_holder, insurance.insured_holder, insurance.gender, insurance.dob, insurance.sum_assured, insurance.address1, insurance.mobile, insurance.telephone, insurance.payment_frequency, insurance.annual_premium, insurance.modal_premium, insurance.collected_premium, insurance.application_number, insurance.remarks, insurance.awb_number, insurance.physical_date, insurance.courier]
        else
          csv << [insurance.transaction_date.strftime("%d-%m-%Y"), insurance.policy_type, insurance.policyname.name, '', 'SARK System India Limited', 'SARK Systems India Limited', 'PATNA', '', '', insurance.transaction_id, UserProfile.find(:first, :conditions => {:user_id=>insurance.user_id}).vle_code, '', '', '', '', '', '', '', '', '', '', '', insurance.collected_premium, insurance.policy_number, insurance.remarks, '', '', '']
        end
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def insu_suspense_collection
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @insurances = Insurance.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date, :suspense_status => true)

    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape', :page_size => 'Legal')
#       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Insurance EOD Transactions.pdf", :type => 'application/pdf')
  end

############################ END INSURANCE REPORTS ###################################################

############################ PANCARD REPORTS ###################################################
  def pancards
    @pancards = Pancard.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
  end

  def pancard_eodreport
    @pancards = Pancard.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
  end

  def pancard_eodreport_export
    @pancards = Pancard.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
    outfile = "PAN Card EOD Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Application No", "Name", "Father Name", "DOB", "Gender", "Address", "Place", "PIN", "District", "State", "Mobile", "Amount", "VLE ID", "VLE NAME", "Payment Mode"]
      @pancards.each do |pancard|
        csv << [pancard.transaction_date.strftime("%d-%m-%Y"), pancard.panapplication_number, pancard.first_name + pancard.middle_name + pancard.last_name, pancard.father_name, pancard.dob, pancard.gender, pancard.address1, pancard.place, pancard.pincode, pancard.district.name, pancard.state.name, pancard.mobile, pancard.amount, UserProfile.find(:first, :conditions => {:user_id=>pancard.user_id}).vle_code, User.find(:first, :conditions => {:id=>pancard.user_id}).name, pancard.payment_mode]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def pancard_daterangereport
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @pancards = Pancard.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
    else
      @pancards = Pancard.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date)).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def pancard_daterangexport
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @pancards = Pancard.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date))
    outfile = "PAN Date Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Application No", "Name", "Father Name", "DOB", "Gender", "Address", "Place", "PIN", "District", "State", "Mobile", "Amount", "VLE ID", "VLE NAME", "Payment Mode"]
      @pancards.each do |pancard|
        csv << [pancard.transaction_date.strftime("%d-%m-%Y"), pancard.panapplication_number, pancard.first_name + pancard.middle_name + pancard.last_name, pancard.father_name, pancard.dob, pancard.gender, pancard.address1, pancard.place, pancard.pincode, pancard.district.name, pancard.state.name, pancard.mobile, pancard.amount, UserProfile.find(:first, :conditions => {:user_id=>pancard.user_id}).vle_code, User.find(:first, :conditions => {:id=>pancard.user_id}).name, pancard.payment_mode]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def pancard_export
    @pancards = Pancard.all
    outfile = "PAN Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Transaction Date", "Application No", "Name", "Father Name", "DOB", "Gender", "Address", "Place", "PIN", "District", "State", "Mobile", "Amount", "VLE ID", "VLE NAME", "Payment Mode"]
      @pancards.each do |pancard|
        csv << [pancard.transaction_date.strftime("%d-%m-%Y"), pancard.panapplication_number, pancard.first_name + pancard.middle_name + pancard.last_name, pancard.father_name, pancard.dob, pancard.gender, pancard.address1, pancard.place, pancard.pincode, pancard.district.name, pancard.state.name, pancard.mobile, pancard.amount, UserProfile.find(:first, :conditions => {:user_id=>pancard.user_id}).vle_code, User.find(:first, :conditions => {:id=>pancard.user_id}).name, pancard.payment_mode]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def pancard_eodpayment
    @pancards = Pancard.where(:transaction_date=>Date.today)
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "PAN EOD Transactions.pdf", :type => 'application/pdf')

  end

  def pancard_datepayment
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @pancards = Pancard.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date)

    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "PAN EOD Transactions.pdf", :type => 'application/pdf')
  end

############################ END PANCARD REPORTS ###################################################
############################ FUNDS REPORTS ###################################################
  def funds
    @deposits = Deposit.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
  end

  def fund_eodreport
    @deposits = Deposit.where(:created_at=>Date.today).paginate(:page =>page, :per_page=>per_page)
  end

  def fund_eodreport_export
    @deposits = Deposit.where(:created_at=>Date.today).paginate(:page =>page, :per_page=>per_page)
    outfile = "Deposit EOD Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Transaction Date", "Transaction ID", "Amount", "Payment Mode", "VLE ID", "VLE Name", "Deposited By"]
      @deposits.each do |deposit|
        csv << [deposit.status, deposit.transaction_date.strftime("%d-%m-%Y"), deposit.transaction_number, deposit.amount, deposit.transaction_mode, UserProfile.find(:first, :conditions => {:user_id=>deposit.user_id}).vle_code, User.find(:first, :conditions => {:id=>deposit.user_id}).name, User.find(deposit.created_by).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def fund_daterangereport
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @deposits = Deposit.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
    else
      @deposits = Deposit.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date)).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def fund_daterangexport
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @deposits = Deposit.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date))
    outfile = "Deposit EOD Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Transaction Date", "Transaction ID", "Amount", "Payment Mode", "VLE ID", "VLE Name", "Deposited By"]
      @deposits.each do |deposit|
        csv << [deposit.status, deposit.transaction_date.strftime("%d-%m-%Y"), deposit.transaction_number, deposit.amount, deposit.transaction_mode, UserProfile.find(:first, :conditions => {:user_id=>deposit.user_id}).vle_code, User.find(:first, :conditions => {:id=>deposit.user_id}).name, User.find(deposit.created_by).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def fund_export
    @deposites = Deposit.all
    outfile = "Deposit Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Deposit Status", "Transaction Date", "Transaction ID", "Amount", "Payment Mode", "VLE ID", "VLE Name", "Deposited By"]
      @deposites.each do |deposit|
        csv << [deposit.status? ? 'Approved' : 'Pending', deposit.fundstatus? ? 'Deposited' : 'Not Deposited', deposit.transaction_date.strftime("%d-%m-%Y"), deposit.transaction_number, deposit.amount, deposit.transaction_mode, UserProfile.find(:first, :conditions => {:user_id=>deposit.user_id}).vle_code, User.find(:first, :conditions => {:id=>deposit.user_id}).name, User.find(deposit.created_by).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def fund_eodpayment
    @deposits = Deposit.where(:transaction_date=>Date.today)
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Deposit EOD Transactions.pdf", :type => 'application/pdf')

  end

  def fund_datepayment
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @deposits = Deposit.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date)

    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Deposit Date Transactions.pdf", :type => 'application/pdf')

  end

############################ END FUNDS REPORTS ###################################################

############################ Education REPORTS ###################################################
  def educations
    @educations = Deducation.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
  end

  def education_eodreport
    @educations = Deducation.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
  end

  def education_eodreport_export
    @educations = Deducation.where(:transaction_date=>Date.today).paginate(:page =>page, :per_page=>per_page)
    outfile = "Education EOD Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Transaction Date", "Application No", "Course", "Amount", "VLE ID", "VLE NAME"]
      @educations.each do |education|
        csv << [education.status? ? 'Approved' : 'Pending', education.created_at.strftime("%d-%m-%Y"), education.transaction_id, education.course.name, education.course.fee, UserProfile.find(:first, :conditions => {:user_id=>education.user_id}).vle_code, User.find(:first, :conditions => {:id=>education.user_id}).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def education_daterangereport
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    if params[:start_date] == nil && params[:end_date] == nil
      @educations = Deducation.order("created_at DESC").paginate(:page =>page, :per_page=>per_page)
    else
      @educations = Deducation.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date)).paginate(:page =>page, :per_page=>per_page)
    end

    respond_to do |format|
      format.html
    end
  end

  def education_daterangexport
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @educations = Deducation.where(:transaction_date => (params[:start_date].to_date)..(params[:end_date].to_date))
    outfile = "Education Date Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
      csv << ["Status", "Transaction Date", "Application No", "Course", "Amount", "VLE ID", "VLE NAME"]
      @educations.each do |education|
        csv << [education.status? ? 'Approved' : 'Pending', education.created_at.strftime("%d-%m-%Y"), education.transaction_id, education.course.name, education.course.fee, UserProfile.find(:first, :conditions => {:user_id=>education.user_id}).vle_code, User.find(:first, :conditions => {:id=>education.user_id}).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def education_export
    @educations = Deducation.all
    outfile = "Education Report" + Time.now.strftime("%d-%m-%Y") + ".csv"
    csv_data = CSV.generate do |csv|
#      csv << ["Transaction Date", "Application No", "Name", "Father Name", "DOB", "Gender", "Address", "Place", "PIN", "District", "State", "Mobile", "Amount", "VLE ID", "VLE NAME", "Payment Mode"]
      csv << ["Status", "Transaction Date", "Application No", "Course", "Amount", "VLE ID", "VLE NAME"]
      @educations.each do |education|
        csv << [education.status? ? 'Approved' : 'Pending', education.created_at.strftime("%d-%m-%Y"), education.transaction_id, education.course.name, education.course.fee, UserProfile.find(:first, :conditions => {:user_id=>education.user_id}).vle_code, User.find(:first, :conditions => {:id=>education.user_id}).name]
      end
    end
    send_data csv_data,
              :type => 'text/csv; charset=iso-8859-1; header=present',
              :disposition => "attachment; filename=#{outfile}"
  end

  def education_eodpayment
    @educations = Deducation.where(:transaction_date=>Date.today)
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Education EOD Transactions.pdf", :type => 'application/pdf')

  end

  def education_datepayment
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @educations = Deducation.where(:transaction_date => params[:start_date].to_date..params[:end_date].to_date)

    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Education EOD Transactions.pdf", :type => 'application/pdf')

  end
############################ END EDUCATION REPORTS ###################################################

end
