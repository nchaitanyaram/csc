#Author: Chaitanya
#Controller: Divisions
#================================================
class DivisionsController < ApplicationController
  layout "admin"
  before_filter :require_user,:recent_items
  filter_access_to :all
  
  helper_method :sort_column, :sort_direction
  def index
    @divisions = Division.search(params[:search]).order(sort_column + ' ' + sort_direction).paginate(:page =>page,:per_page=>per_page )
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @divisions }
      format.js
    end
  end
  
  def show
    @division = Division.find(params[:id])
    render :layout => "application"
  end
  
  def new
    @division = Division.new
    @states = State.all
    render :layout => "application"
  end
  
  def edit
    @division = Division.find(params[:id])
    @states = State.all
    render :layout => "application"
  end
  
  def create
    @division = Division.new(params[:division])
    @division.created_by = @created_by
    
    respond_to do |format|
      if @division.save
        format.html { redirect_to(@division, :notice => 'Division was successfully created.') }
        format.xml  { render :xml => @division, :status => :created, :location => @division }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @division.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def update
    @division = Division.find(params[:id])
    @division.updated_by = @created_by
    
    respond_to do |format|
      if @division.update_attributes(params[:division])
        format.html { redirect_to(@division, :notice => 'Division was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @division.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @division = Division.find(params[:id])
    @division.destroy
    
    respond_to do |format|
      format.html { redirect_to(divisions_url) }
      format.xml  { head :ok }
    end
  end
  def export
    @divisions = Division.all
    html = render_to_string :layout => false
    kit  = PDFKit.new(html,:page_size => 'Legal')
 #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Divisions.pdf", :type => 'application/pdf')

  end
################################################################################################################
  private
  def recent_items
    @recent_divisions = Division.order('created_at DESC').limit(5)
  end
  def sort_column
    Division.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
################################################################################################################
end
