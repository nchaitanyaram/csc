#Author: Chaitanya
#===================================
class BlocksController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @blocks = Block.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @blocks }
      format.js
    end
  end

  def show
    @block = Block.find(params[:id])
    render :layout => "application"
  end

  def new
    @block = Block.new
    @states = State.all
    @divisions, @districts = Division.all, []
    render :layout => "application"  end

  def edit
    @block = Block.find(params[:id])
    @states = State.all
    @divisions = Division.select('id,name').where(:state_id=>@block.state_id)
    @districts = District.select('id,name').where(:division_id=>@block.division_id)
    render :layout => "application"
  end

  def create
    puts "################   #{params[:block][:division_id]}"
    @states = State.all
    @divisions = Division.all
    @districts = District.where(:division_id=>params[:block][:division_id])
    @block = Block.new(params[:block])
    @block.created_by = current_user.id

    respond_to do |format|
      if @block.save
        format.html { redirect_to(@block, :notice => 'Block was successfully created.') }
        format.xml { render :xml => @block, :status => :created, :location => @block }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @block.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @block = Block.find(params[:id])
    @states = State.all
    @divisions = Division.select('id,name').where(:state_id=>params[:block][:state_id])
    @districts = District.select('id,name').where(:division_id=>params[:block][:division_id])
    @block.updated_by = current_user.id
    respond_to do |format|
      if @block.update_attributes(params[:block])
        format.html { redirect_to(@block, :notice => 'Block was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @block.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @block = Block.find(params[:id])
    @block.destroy

    respond_to do |format|
      format.html { redirect_to(blocks_url) }
      format.xml { head :ok }
    end
  end

  ## used to dynamically load districts based on divisions in new and edit views
  def load_districts
    @districts = params[:id] != "" ? District.select('id,name').where("division_id = ?", params[:id].to_i) : []
    respond_to do |format|
      format.js
    end
  end

  def export
    @blocks = Block.all
    html = render_to_string :layout => false
    kit  = PDFKit.new(html,:page_size => 'Legal')
 #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Blocks.pdf", :type => 'application/pdf')

  end
############################# Private
  private

  def recent_items
    @recent_blocks = Block.order('created_at DESC').limit(5)
  end

  def sort_column
    Block.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
###########################################
end
