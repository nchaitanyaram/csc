class TicketsController < ApplicationController
  layout "admin"
  before_filter :require_user,:recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
   @tickets = Ticket.where(:created_by=>current_user.id).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page)if has_role?(:vle)
   @tickets = Ticket.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page )if has_role?(:admin)or has_role?(:state_engineer)
   @tickets = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @tickets }
      format.js
    end
  end

  def show
    @ticket = Ticket.find(params[:id])
    render :layout => "application"
  end

  def new
    @ticket = Ticket.new
    render  :layout => "application"
  end

  def edit
    @ticket = Ticket.find(params[:id])
  end

  def create
    @ticket = Ticket.new(params[:ticket])
    @ticket.created_by = @ticket.user_id = @created_by
    @ticket.open_date = Date.today
    @ticket.ticket_number = 'SUP/'+  current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s
 
    respond_to do |format|
      if @ticket.save
        format.html { redirect_to(@ticket, :notice => 'Support Ticket was successfully created.') }
        format.xml  { render :xml => @ticket, :status => :created, :location => @ticket }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ticket.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @ticket = Ticket.find(params[:id])
    @ticket.updated_by = @created_by

    respond_to do |format|
      if @ticket.update_attributes(params[:ticket])
        format.html { redirect_to(@ticket, :notice => 'Support Ticket was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ticket.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy

    respond_to do |format|
      format.html { redirect_to(tickets_url) }
      format.xml  { head :ok }
    end
  end
  
  def update_status
    ticket = Ticket.find(params[:id])    
    ticket.update_attributes(:status=>(ticket.status ? false : true),:close_date=>Date.today )
    flash[:success] = "Support Ticket Status has been updated."
    respond_to do |format|
      format.html{redirect_to(tickets_url) }     
    end
  end
##################################################  
  private
  def recent_items
   @recent_tickets = Ticket.where(:created_by=>current_user.id).search(params[:search]).order('tickets.created_at DESC').limit(5) if has_role?(:vle)
   @recent_tickets = Ticket.order('tickets.created_at DESC').limit(5) if has_role?(:admin) or has_role?(:state_engineer)
   @recent_tickets = Ticket.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).order('tickets.created_at DESC').limit(5) if has_role?(:district_manager) or has_role?(:district_engineer)

  end
  
  def sort_column
    State.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
