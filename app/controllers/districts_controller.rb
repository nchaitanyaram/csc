#Created By: Chaitanya
#Date: 25 May 2011
#Controller: Districts
#=================================================
class DistrictsController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @districts = District.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @districts }
      format.js
    end
  end

  def show
    @district = District.find(params[:id])
    render :layout => "application"
  end

  def new
    @district = District.new
    @states = State.all
    @divisions = Division.all
    render :layout => "application"
  end

  def edit
    @district = District.find(params[:id])
    @states = State.all
    @divisions = Division.all
    render :layout => "application"
  end

  def create
    @district = District.new(params[:district])
    @district.created_by = @created_by

    respond_to do |format|
      if @district.save
        format.html { redirect_to(@district, :notice => 'District was successfully created.') }
        format.xml { render :xml => @district, :status => :created, :location => @district }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @district.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @district = District.find(params[:id])
    @district.updated_by = @updated_by

    respond_to do |format|
      if @district.update_attributes(params[:district])
        format.html { redirect_to(@district, :notice => 'District was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @district.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @district = District.find(params[:id])
    @district.destroy

    respond_to do |format|
      format.html { redirect_to(districts_url) }
      format.xml { head :ok }
    end
  end

  def export
    @districts = District.all
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :page_size => 'Legal')
    #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "Districts.pdf", :type => 'application/pdf')

  end

################################################################################################################
  private
  def recent_items
    @recent_districts = District.order('created_at DESC').limit(5)
  end

  def sort_column
    District.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
################################################################################################################
end
