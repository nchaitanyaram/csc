class ShortmessagesController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @sms = Shortmessage.where(:created_by=>current_user.id).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle)
    @sms = Shortmessage.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @sms = Shortmessage.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)
    @sms = Shortmessage.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @shortmessages }
      format.js

    end
  end

  def show
    @shortmessage = Shortmessage.find(params[:id])
    #render :layout => "application"
    render :layout => nil
  end

  def new
    @shortmessage = Shortmessage.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @shortmessage }
    end
  end

  def edit
    @shortmessage = Shortmessage.find(params[:id])
  end

  def create
    @shortmessage = Shortmessage.new(params[:shortmessage])

    respond_to do |format|
      if @shortmessage.save
        format.html { redirect_to(@shortmessage, :notice => 'Shortmessage was successfully created.') }
        format.xml  { render :xml => @shortmessage, :status => :created, :location => @shortmessage }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @shortmessage.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @shortmessage = Shortmessage.find(params[:id])

    respond_to do |format|
      if @shortmessage.update_attributes(params[:shortmessage])
        format.html { redirect_to(@shortmessage, :notice => 'Shortmessage was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shortmessage.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @shortmessage = Shortmessage.find(params[:id])
    @shortmessage.destroy

    respond_to do |format|
      format.html { redirect_to(shortmessages_url) }
      format.xml  { head :ok }
    end
  end

  def export
    @shortmessages = Shortmessage.all
    html = render_to_string :layout => false
    kit = PDFKit.new(html, :orientation => 'Landscape') #,:page_size => 'Legal')
                                                        #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "SMS Transactions.pdf", :type => 'application/pdf')

  end

####################################### Private ####################################
    private
  def recent_items
    @recent_sms = Shortmessage.where(:created_by=>current_user.id).order('created_at DESC').limit(5) if has_role?(:vle)
    @recent_sms = Shortmessage.order('created_at DESC').limit(5) if has_role?(:admin)
    @recent_sms = Shortmessage.order('created_at DESC').limit(5) if has_role?(:manager)
    @recent_sms = Shortmessage.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).order('shortmessages.created_at DESC').limit(5) if has_role?(:district_manager) or has_role?(:district_engineer)
  end

  def sort_column
    Shortmessage.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
################################### End Private ####################################
end
