class PolicynamesController < ApplicationController
  layout "admin" 
  before_filter :require_user,:recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction
  def index
    @policynames = Policyname.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page )
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @policynames }
      format.js
    end
  end
  
  def show
    @policyname = Policyname.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @policyname }
    end
  end
  
  def new
    @policyname = Policyname.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @policyname }
    end
  end
  
  def edit
    @policyname = Policyname.find(params[:id])
  end
  
  def create
    @policyname = Policyname.new(params[:policyname])
    @policyname.created_by = @created_by
    
    respond_to do |format|
      if @policyname.save
        format.html { redirect_to(@policyname, :notice => 'Policy was successfully created.') }
        format.xml  { render :xml => @policyname, :status => :created, :location => @policyname }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @policyname.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def update
    @policyname = Policyname.find(params[:id])
    @policyname.updated_by = @created_by
    
    respond_to do |format|
      if @policyname.update_attributes(params[:policyname])
        format.html { redirect_to(@policyname, :notice => 'Policy was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @policyname.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @policyname = Policyname.find(params[:id])
    @policyname.destroy
    
    respond_to do |format|
      format.html { redirect_to(policynames_url) }
      format.xml  { head :ok }
    end
  end
  private
  def recent_items
    @recent_policynames = Policyname.order('created_at DESC').limit(5)
  end
  
  def sort_column
    Policyname.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
