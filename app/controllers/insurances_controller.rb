#Author: Chaitanya
#Date: June 04 2011
#========================================
class InsurancesController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction
######## For SMS Gateway ##############
  require 'net/http'
  require 'uri'
####################################
  def index
    @insurances = Insurance.where(:created_by=>current_user.id,:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances = Insurance.where(:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @insurances = Insurance.where(:suspense_status => false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)
    @insurances = Insurance.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:suspense_status=>false).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @insurances }
      format.js
    end
  end

  def show
    @insurance = Insurance.find(params[:id])
    render :layout => "application"
  end

  def new
    @insurance = Insurance.new
    render :layout => "application"
  end

  def edit
    @insurance = Insurance.find(params[:id])
  end

  def create
    @insurance = Insurance.new(params[:insurance])
    @insurance.created_by = @insurance.user_id = current_user.id

    @insurance.transaction_id = 'INSU/' + current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s

    @user_profile_user = UserProfile.find(:first, :conditions=>{:user_id=>current_user.id})
    user_profile_user_funds = @user_profile_user.available_funds

    if user_profile_user_funds <=0
      flash[:error] = "Sorry! You have No Funds to perform Transaction."
      redirect_to(insurances_url)
    else
      user_profile_remaining_funds = user_profile_user_funds - params[:insurance][:collected_premium].to_f #transaction_records_sum

      if user_profile_remaining_funds < 0
        flash[:error] = "Sorry! You have Insufficient Funds.Transaction is Cancelled."
        redirect_to(insurances_url)
      else
        respond_to do |format|
          if @insurance.save
            @insurance_transaction = Transaction.new(:user_id=>current_user.id, :transaction_number=>@insurance.transaction_id, :transaction_date=>Date.today, :transaction_mode=>'Dr', :service=>"Insurance", :amount=>params[:insurance][:collected_premium])
            @insurance_transaction.save
            @user_profile_user.available_funds = user_profile_remaining_funds
            @user_profile_user.save

            sms_status ### Send SMS

            format.html { redirect_to(@insurance, :notice => 'Insurance was successfully created.') }
            format.xml { render :xml => @insurance, :status => :created, :location => @insurance }
          else
            format.html { render :action => "new" }
            format.xml { render :xml => @insurance.errors, :status => :unprocessable_entity }
          end
        end
      end
    end

  end

  def update
    @insurance = Insurance.find(params[:id])
    @insurance.updated_by = @created_by

    respond_to do |format|
      if @insurance.update_attributes(params[:insurance])
        format.html { redirect_to(@insurance, :notice => 'Insurance was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @insurance.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @insurance = Insurance.find(params[:id])
    @insurance.destroy

    respond_to do |format|
      format.html { redirect_to(insurances_url) }
      format.xml { head :ok }
    end
  end

  def print
    @insurance = Insurance.find(params[:id])

    #conten for QR Code
    @qrcode = @insurance.transaction_id + ',' + @insurance.transaction_date.strftime('%d-%m-%Y') + ',' + @insurance.collected_premium.to_s + ',' + UserProfile.find_by_user_id(@insurance.user_id).vle_code + ',' + UserProfile.find_by_user_id(@insurance.user_id).panchayat + '/' + UserProfile.find_by_user_id(@insurance.user_id).district.name + ',' + UserProfile.find_by_user_id(@insurance.user_id).district_id.to_s + UserProfile.find_by_user_id(@insurance.user_id).state_id.to_s + @insurance.user_id.to_s + UserProfile.find_by_user_id(@insurance.user_id).id.to_s

    #PDF Render
    html = render_to_string :layout => false
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css'
    send_data(kit.to_pdf, :filename => "Insurance Verified Receipt-#{@insurance.transaction_id}_Verified.pdf", :type => 'application/pdf')
  end

  def receipt
    @insurance = Insurance.find(params[:id])

    #conten for QR Code
    @qrcode = @insurance.transaction_id + ',' + @insurance.transaction_date.strftime('%d-%m-%Y') + ',' + @insurance.collected_premium.to_s + ',' + UserProfile.find_by_user_id(@insurance.user_id).vle_code + ',' + UserProfile.find_by_user_id(@insurance.user_id).panchayat + '/' + UserProfile.find_by_user_id(@insurance.user_id).district.name + ',' + UserProfile.find_by_user_id(@insurance.user_id).district_id.to_s + UserProfile.find_by_user_id(@insurance.user_id).state_id.to_s + @insurance.user_id.to_s + UserProfile.find_by_user_id(@insurance.user_id).id.to_s

    #PDF Render
    html = render_to_string :layout => false
    kit = PDFKit.new(html)
    kit.stylesheets << RAILS_ROOT + '/public/stylesheets/styles.css'
    send_data(kit.to_pdf, :filename => "Insurance Receipt-#{@insurance.transaction_id}.pdf", :type => 'application/pdf')
  end

  def courier
    @insurance = Insurance.find(params[:id])
    render :layout => "application"
  end

  def courier_update
    @insurance = Insurance.find(params[:id])
    @insurance.awb_number = params[:insurance][:awb_number]
    @insurance.physical_date = params[:insurance][:physical_date]
    @insurance.courier = params[:insurance][:courier]

    if  @insurance.save
      flash[:success] = "Courier Details Updated."
      redirect_to(insurances_url)
    else
      render 'courier', :id=>params[:id]
    end
  end

  def verifyinsurance
    @insurances = Insurance.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page)
  end

  def update_status
    insurance = Insurance.find(params[:id])
    insurance.update_attribute('status', insurance.status ? false : true)
    flash[:success] = "Insurance Transaction Status has been updated."
    respond_to do |format|
      format.html { redirect_to(:controller=>'insurances', :action=>'index') }
    end
  end

  def renewpolicy
    @insurance = Insurance.new
    render :layout => "application"
  end

  def createrenew
    @insurance = Insurance.new(params[:insurance])
    @insurance.created_by = @insurance.user_id = current_user.id

    @insurance.transaction_id = 'INSU/' + current_user.id.to_s + '/' + Time.now.strftime('%y%m%d%H%M%S').to_s

    # puts @insurance.instrument_date= params[:insurance][:instrument_date]

    @user_profile_user = UserProfile.find(:first, :conditions=>{:user_id=>current_user.id})
    user_profile_user_funds = @user_profile_user.available_funds

    if user_profile_user_funds <=0
      flash[:error] = "Sorry! You have No Funds to perform Transaction.Please deposit Funds to continue the Transactions"
      redirect_to(insurances_url)
    else
      user_profile_remaining_funds = user_profile_user_funds - params[:insurance][:collected_premium].to_f #transaction_records_sum

      if user_profile_remaining_funds < 0
        flash[:error] = "Sorry! You have Insufficient Funds.Transaction is Cancelled. Please deposit Funds to continue the Transactions"
        redirect_to(insurances_url)
      else
        respond_to do |format|
          if @insurance.save
            @insurance_transaction = Transaction.new(:user_id=>current_user.id, :transaction_number=>@insurance.transaction_id, :transaction_date=>Date.today, :transaction_mode=>'Dr', :service=>"Insurance", :amount=>params[:insurance][:collected_premium])
            @insurance_transaction.save
            @user_profile_user.available_funds = user_profile_remaining_funds
            @user_profile_user.save

            sms_status # sent SMS

            format.html { redirect_to(@insurance, :notice => 'Insurance was successfully created.') }
            format.xml { render :xml => @insurance, :status => :created, :location => @insurance }
          else
            format.html { render :action => "new" }
            format.xml { render :xml => @insurance.errors, :status => :unprocessable_entity }
          end
        end
      end
    end
  end

  def payment_mode
    respond_to do |format|
      format.js
    end
  end

  def sms_status
    mobile = @insurance.mobile
    #name = @pancard.first_name + @pancard.last_name
    #message = "Dear+Customer,+A Premium+of+Rs."+@insurance.collected_premium.to_s+"/-+received+towards+"+@insurance.policyname.name+"+Policy.+Your+Reference+No:+"+@insurance.transaction_id+"."
    if @insurance.policy_type == "New"
      message = "Dear Customer, A Premium of Rs." + @insurance.collected_premium.to_s + "/- received towards New "+ @insurance.policyname.name + "Policy. Your Reference No:"+ @insurance.transaction_id + "."
    else
      message = "Dear Customer, A Premium of Rs." + @insurance.collected_premium.to_s + "/- received towards Renewal of" + @insurance.policyname.name + "Policy. Your Reference No:"+ @insurance.transaction_id + "."
    end
    url = "http://voice.full2ads.co.cc/api/sms.php?uid=6368616974616e7961&pin=4e4ce8b0a500c&sender=SARKCSC&route=1&mobile=#{mobile},9934186169&message=#{message}"
    Net::HTTP.get_print URI.parse(URI.encode(url.strip))

    @insurance_sms = Shortmessage.new(:user_id=>current_user.id, :transaction_id=>@insurance.transaction_id, :service=>'Insurance', :message=>message, :mobile=>mobile)
    @insurance_sms.save

  end

  def suspense
    insurance = Insurance.find(params[:id])
    insurance.update_attribute('suspense_status', insurance.suspense_status ? false : true)
    if insurance.suspense_status?
    flash[:success] = "Insurance Transaction is Suspended."
    respond_to do |format|
      format.html { redirect_to(:controller=>'insurances', :action=>'index') }
    end

    else
    flash[:success] = "Insurance Transaction is Reverted successfully"
    respond_to do |format|
      format.html { redirect_to(:controller=>'insurances', :action=>'suspense_transactions') }
    end

    end

  end
  def suspense_transactions
    @insurances = Insurance.where(:created_by=>current_user.id,:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:vle) or has_role?(:dlf_agent)
    @insurances = Insurance.where(:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:admin)
    @insurances = Insurance.where(:suspense_status => true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:manager)
    @insurances = Insurance.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}},:suspense_status=>true).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @insurances }
      format.js
    end

  end

  #########################################  Private Methods #######################################################
  private
  def recent_items
    @recent_insurances = Insurance.where(:created_by=>current_user.id).order('created_at DESC').limit(5) if has_role?(:vle) or has_role?(:dlf_agent)
    @recent_insurances = Insurance.order('created_at DESC').limit(5) if has_role?(:admin)
    @recent_insurances = Insurance.order('created_at DESC').limit(5) if has_role?(:manager)
    @recent_insurances = Insurance.joins({:user=>:user_profile}).where({:user=>{:user_profile=>{:district_id=>current_user.user_profile.district_id}}}).order('insurances.created_at DESC').limit(5) if has_role?(:district_manager) or has_role?(:district_engineer)

  end

  def sort_column
    Insurance.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
  ######################################## Private Methods ############################################################
end
