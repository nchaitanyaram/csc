#Created :chaitanya
#date: 25 May 2011
#====================================
class StatesController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @states = State.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page, :per_page=>per_page)
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @states }
      format.js
    end
  end

  def show
    @state = State.find(params[:id])
    render :layout => "application"

  end

  def new
    @state = State.new
    render :layout => "application"
  end

  def edit
    @state = State.find(params[:id])
    render :layout => "application"
  end

  def create
    @state = State.new(params[:state])
    @state.created_by = @created_by
    respond_to do |format|
      if @state.save
        format.html { redirect_to(@state, :notice => 'State was successfully created.') }
        format.xml { render :xml => @state, :status => :created, :location => @state }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @state.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @state = State.find(params[:id])
    @state.updated_by = @created_by
    respond_to do |format|
      if @state.update_attributes(params[:state])
        format.html { redirect_to(@state, :notice => 'State was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @state.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @state = State.find(params[:id])
    @state.destroy

    respond_to do |format|
      format.html { redirect_to(states_url) }
      format.xml { head :ok }
    end
  end
  def export
    @states = State.all
    html = render_to_string :layout => false
    kit  = PDFKit.new(html,:page_size => 'Legal')
 #       kit.stylesheets << RAILS_ROOT + '/public/stylesheets/style.css'
    send_data(kit.to_pdf, :filename => "States.pdf", :type => 'application/pdf')

  end
##################################################################################################################
  private
  def recent_items
    @recent_states = State.order('created_at DESC').limit(5)
  end

  def sort_column
    State.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
##################################################################################################################
end
