#Created and Updated: Chaitanya
#Updated on: 24 May 2011
#=============================================================
require 'csv'
class UsersController < ApplicationController
  layout "admin"
  helper_method :sort_column, :sort_direction
  before_filter :require_user,:recent_items #, :only => [:index,:show, :edit, :update]
  filter_access_to :all
  before_filter :restricted_roles,:only=>[:new,:edit,:create,:update]
  
  def index
    #Getting the users having roles with VLE and CSP(admin can create or update only the user having the roles VLE nd CSP)
    @users = User.admin_users.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:admin)
    @users = User.super_admin_users.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:super_admin)
    @users = User.admin_users.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:manager)
    @users = User.joins([:user_profile]).where(:user_profile=>{:district_id => current_user.user_profile.district_id}).search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:district_manager) or has_role?(:district_engineer)
  end
  
  def new
    @user = User.new
    @user.build_user_profile if has_role?(:admin)
  end
  
  def create
    @user = User.new(params[:user])
    @user.created_by = @created_by
    @user.password = @user.password_confirmation = params[:user][:email]

    if has_role?(:admin)
    state =State.find(params[:user][:user_profile_attributes][:state_id])
    district =District.find(params[:user][:user_profile_attributes][:district_id])
    district.increment!(:dis_number)
    @user.user_profile.vle_code= state.short_code + district.short_code+"%04d" % district.dis_number
    end

    if has_role?(:super_admin)
      @user.approved= 1
    end

    respond_to do |format|
      if @user.save
        format.html { redirect_to(users_path, :notice => 'User was successfully created.') }
      else
        format.html { render :action => "new" }
      end
    end

  end
  
  def show
    @user = User.find(params[:id])
    render :layout => "application"
  end
  
  def edit
    @user = User.find(params[:id])
    render :layout => "application"
  end
  
  def update
    @user = User.find(params[:id])
    #    params[:user][:role_ids] ||= []
    @user.updated_by = @updated_by
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to(users_path, :notice => 'User was successfully updated.') }
      else
        format.html { render :action => "edit" }
      end
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    
    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end
  
  #Method to approve user at manager level
  def verifyuser
    @users = User.admin_users.order(sort_column + " " + sort_direction).paginate(:page =>page,:per_page=>per_page) if has_role?(:manager)
  end
  #Method to update user status ie Block or Approved at Manager Level
  def update_status
    user = User.find(params[:id])
    user.update_attribute('approved',user.approved? ? false : true )
    flash[:success] = "User Status has been updated."   
    respond_to do |format|
      format.html{redirect_to(:controller=>'users',:action=>'verifyuser') }     
    end
  end
  
  #Method to upload documents of User by Admin
  def document
    user = User.find(params[:id])
    @user_profile = user.user_profile
    render :layout => "application"
#    render :layout => nil
  end
  #Method to upload documents
  def upload_document
    @user_profile = UserProfile.find(params[:id])
    
    respond_to do |format|
      if @user_profile.update_attributes(params[:user_profile])
        format.html { redirect_to(users_path, :notice => 'Documents Uploaded Successfully.') }
      else
        format.html { render :action => "document",:id=>params[:user_id] }
      end
    end
  end
  
  #Method to dispaly common details of user
  def profile
    @user = User.find(params[:id])
    render :layout => "application"
  end
  #Method to edit common details of the user
  def editprofile
    @user = User.find(params[:id])
    render :layout => "application"
  end
  #Method to update the common details of the user
  def updateprofile
    @user = User.find(params[:id])
    @user.updated_by = @updated_by
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { render :action => "profile",:id=>params[:id],:layout=>"application" }
        flash[:success] = "Profile updated Successfully."
      else
        format.html { render :action => "profile",:id=>params[:id] }
      end
    end  
  end
  
  def upload
    render :layout => "application"
  end
  
  def csv_import
    csv_file = params[:upload][:file]                      ### un comment for Ruby 1.9
    #    @user_imp=CSV::Reader.parse(params[:upload][:file])   ### uncomment for Ruby 1.8
    n=0
    #    @user_imp.each do |row|                               #Ruby 1.8
    CSV.new(csv_file.tempfile,:col_sep => ",").each do |row|   
      user = User.create do |u|
        u.name  =row[0]
        u.email = row[1]
        u.password = u.password_confirmation = row[1]
        u.mobile_number = row[2]
        u.approved = true
        u.created_by = 2
        u.updated_by=2
      end
      if user.save(:validate => false)
        Assignment.create(:user_id=>user.id,:role_id=>4,:created_at=>Time.now,:updated_at=>Time.now)        
        c=UserProfile.new  
        c.state_id= 1 
        c.user_id = user.id
        division = Division.find_by_name(row[4].strip)
        c.division_id=division.id
        district=District.find_by_name(row[5].strip)
        c.district_id =district.id
        block=Block.find_by_name(row[6].strip)
        c.block_id = block.id
        c.panchayat=row[7]
        c.save(:validate => false)
        
      end
      n = n + 1
    end
    flash[:notice]= "#{n} Users are imported sucessfully"
    respond_to do |format|
      format.html { redirect_to(users_path) }
      format.xml  { head :ok }
    end
  end
  
  def changepassword
    @user = current_user
    render :layout => "application"
  end
  
  
  def updatepassword
    @user = current_user
    return flash.now[:error] = "Old password is not correct" unless @user.valid_password? params[:old_password]     
    if ((params[:password] == params[:password_confirmation]) && !params[:password_confirmation].blank?)
      @user.password_confirmation = params[:password_confirmation]
      @user.password = params[:password]
      if @user.save
        flash.now[:success] = "Password Changed Successfully."
 #       redirect_to redirect_to(:controller=>'generals',:action=>'index')
      else
        flash.now[:error]= "Password not changed" 
      end
    else
      flash.now[:error] = "New Password mismatch" 
      @old_password = params[:old_password]
    end     
  end 

  def resetpass
    @user = User.find(params[:id])
    @user.password= @user.password_confirmation = @user.email

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to(users_path, :notice => 'User password is Reset.') }
      else
        format.html { redirect_to(users_path, :error => 'User password is Not Reset.') }
      end
    end
  end

    def payment_mode
    respond_to do |format|
      format.js
    end
    end

  def activity
    @user = User.find(params[:id])
    render :layout => nil
  end

  def promote
    @role = Assignment.where(:user_id => params[:id])

    @role.each do |r|
      r.role_id = 4
      r.save
    end
    respond_to do |format|
      format.html { redirect_to(users_path)  }
      flash[:success] = "User promoted to VLE."

    end
  end
  #################################################################################################
  private
  def recent_items
    #    @recent_users = User.order('created_at DESC').limit(5)
   return @recent_users = User.admin_users.order('users.created_at DESC').limit(5) if has_role?(:admin)
   return @recent_users = User.super_admin_users.order('users.created_at DESC').limit(5) if has_role?(:super_admin)
   return @recent_users = User.admin_users.order('users.created_at DESC').limit(5) if has_role?(:manager)
   return @recent_users = User.joins([:user_profile]).where(:user_profile=>{:district_id=>current_user.user_profile.district_id}).order('users.created_at DESC').limit(5) if has_role?(:district_manager) or has_role?(:district_engineer)
  end
  
  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : "users.created_at"
 #   ['users.email', 'users.user_profiles.vle_code'].include?(params[:sort]) ? params[:sort] : 'users.approved'
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
  
  def restricted_roles
    @roles = Role.where(:name=>['admin','super_admin','manager','state_engineer']) if has_role?(:super_admin)
    @roles = Role.where(:name=>['vle','dlf_agent','district_manager','district_engineer']) if  has_role?(:admin)
  end
  
end
