class BulkmailsController < ApplicationController
  layout "admin"
  before_filter :require_user, :recent_items
  filter_access_to :all
  helper_method :sort_column, :sort_direction

  def index
    @bulkmails = Bulkmail.order(sort_column + ' ' + sort_direction).paginate(:page =>page,:per_page=>per_page )

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @bulkmails }
    end
  end

  def show
    @bulkmail = Bulkmail.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @bulkmail }
    end
  end

  def new
    @bulkmail = Bulkmail.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml { render :xml => @bulkmail }
    end
  end

  def edit
    @bulkmail = Bulkmail.find(params[:id])
  end

  def create
    @bulkmail = Bulkmail.new(params[:bulkmail])
    @bulkmail.mail_date = Date.today
    @bulkmail.created_by = current_user.id
    @bulkmail.district_id = params[:district_id]

    if @bulkmail.mail_type == "All"
    @users = User.admin_users
    else
    @users = User.joins([:user_profile]).where(:user_profile=>{:district_id => params[:district_id]})
    end
    respond_to do |format|
      if @bulkmail.save
      @users.each do |user|
      UserMailer.registration_confirmation(user,@bulkmail).deliver
      end
        format.html { redirect_to(@bulkmail, :notice => 'Mail was sent successfully.') }
      else
        format.html { render :action => "new" }
        format.xml { render :xml => @bulkmail.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @bulkmail = Bulkmail.find(params[:id])

    respond_to do |format|
      if @bulkmail.update_attributes(params[:bulkmail])
        format.html { redirect_to(@bulkmail, :notice => 'Bulkmail was successfully updated.') }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @bulkmail.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @bulkmail = Bulkmail.find(params[:id])
    @bulkmail.destroy

    respond_to do |format|
      format.html { redirect_to(bulkmails_url) }
      format.xml { head :ok }
    end
  end

  def mail_mode
    respond_to do |format|
      format.js
    end
  end

  #############################################################################################################
  private

  def recent_items
    @recent_bulkmails = Bulkmail.order('created_at DESC').limit(5)
  end

  def sort_column
    Bulkmail.column_names.include?(params[:sort]) ? params[:sort] : "subject"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  #############################################################################################################

end
