class Deducation < ActiveRecord::Base
  #Relations
  belongs_to :course
  belongs_to :district
  belongs_to :state

  #validations

  #search
  def self.search(search)
    if search
      where(:application_number.matches => "%#{search}%")
    else
      scoped
    end
  end
end
