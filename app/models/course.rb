class Course < ActiveRecord::Base
  #Relations
  has_many :deducations
  belongs_to :university

  #validations

  #search
   def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end
end
