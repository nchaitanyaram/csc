#Author: Chaitanya
#Modified By: Upender and Chaitanya
#Date: 1 June 2011
#========================================
class User < ActiveRecord::Base
  #Authlogic Gem
  acts_as_authentic

  #Relations
  has_many :assignments
  has_many :roles, :through => :assignments

  has_one :user_profile

  has_many :tickets
  has_many :pancards
  has_many :insurances


  accepts_nested_attributes_for :user_profile

  #Validations
  validates :name, :presence =>true
  validates :email, :presence=>true
=begin
  validates_length_of :mobile_number, :maximum => 10
  validates_numericality_of :mobile_number,:only_integer => true
  validates :mobile_number, :presence => true
  validates :address1, :presence=>true
  validates :pin, :presence=>true
  validates_length_of :pin, :maximum => 6
  validates_numericality_of :pin, :greater_than =>800000, :only_integer => true, :less_than_or_equal_to=>899999
=end

  #admin_users
  scope :admin_users, includes([:roles]).where(:roles => {:name =>['vle', 'dlf_agent', 'district_manager', 'district_engineer']})
  ##super admin_users
  scope :super_admin_users, includes([:roles]).where(:roles => {:name =>['admin', 'super_admin', 'manager', 'state_engineer']})

  #Method: Roles
  def role_symbols
    roles.map do |role|
      role.name.underscore.to_sym
    end
  end


  def has_roles?(multi_roles)
    ######## checking equality of Arrays ["B","C"] == ["C","B"] #returns false
    ##########  ["B","C"].sort == ["C","B"].sort #returns true
    roles.map(&:name).sort == multi_roles.sort
  end

  def role_names
    #appending comma(,) to every role except last one
    name = ''
    roles.map(&:name).each { |r| name << r }
    name
  end

  #Avatar
  has_attached_file :avatar, :styles => {:medium => "300x300>", :thumb => "100x100>", :small=>"50x50"}

  #search using meta_where Gem
  def self.search(search)
    if search
      joins({:user_profile=>[:division, :district, :block]}).where(
          {:name.matches => "%#{search}%"} |
              {:user_profile =>{:division=>[:name.matches => "%#{search}%"]}} |
              {:user_profile =>{:district=>[:name.matches => "%#{search}%"]}} |
              {:user_profile =>{:block=>[:name.matches => "%#{search}%"]}} |
              {:user_profile=>[:vle_code.matches=>"%#{search}%"]} |
              {:user_profile=>{:block=>[:name.matches => "%#{search}%"]}} |
              {:user_profile=>[:panchayat.matches=>"%#{search}%"]}
      )
    else
      scoped
    end
  end

end

