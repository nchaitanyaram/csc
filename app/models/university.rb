class University < ActiveRecord::Base
  #Relations
  has_many :course

    #validations

  #search
   def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end
end
