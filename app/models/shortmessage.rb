class Shortmessage < ActiveRecord::Base

  #search using meta_where Gem
  def self.search(search)
    if search
      where({:transaction_id.matches => "%#{search}%"} | {:service.matches => "%#{search}%"} | {:mobile.matches => "%#{search}%"})
    else
      scoped
    end
  end
end
