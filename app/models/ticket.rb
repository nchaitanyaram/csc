#Author: Chaitanya
#===============================
class Ticket < ActiveRecord::Base
  #Associations
  belongs_to :user

   #search using meta_where Gem
  def self.search(search)
    if search
      where(:ticket_number.matches => "%#{search}%")
    else
      scoped
    end
  end
end
