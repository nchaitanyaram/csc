#Author: Chaitanya Ram
#========================================
class Bulkmail < ActiveRecord::Base
  belongs_to :district

  scope :admin_users, includes([:roles]).where(:roles => {:name =>['vle', 'csp', 'district_manager', 'district_engineer']})

end
