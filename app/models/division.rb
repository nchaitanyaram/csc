class Division < ActiveRecord::Base
  #Relations
  belongs_to :state
  has_many :districts,:dependent=>:destroy
  has_many :blocks,:dependent=>:destroy
  has_many :user_profiles
  
  #validations
    validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 100}
  #search using meta_where Gem
  def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end

end
