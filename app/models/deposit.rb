#Author: Chaitanya
#======================
class Deposit < ActiveRecord::Base
  #Relations
  
  #validations
#  validates :amount, :presence => true

  
  #search
   def self.search(search)
    if search
      where(:transaction_number.matches => "%#{search}%")
#      where({:transaction_number.matches => "%#{search}%"} | {:amount.matches => "%#{search}%"}  )
    else
      scoped
    end
  end
  
  #other
    has_attached_file :proof, :styles => { :medium => "300x300>",  :small=>"50x50" }
  # before_proof_post_process :allow_only_images
  #
  #def allow_only_images
  #  if !(attachment.content_type =~ %r{^(image|(x-)?application)/(x-png|pjpeg|jpeg|jpg|png|gif)$})
  #    return false
  #  end
  #end

end
