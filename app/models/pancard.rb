class Pancard < ActiveRecord::Base
  #Relations
  belongs_to :district
  belongs_to :state
  belongs_to :user
  
  #search using meta_where Gem
  def self.search(search)
    if search
      where(:panapplication_number.matches => "%#{search}%")
    else
      scoped
    end
  end
  
  #Upload Documents
  has_attached_file :idproof, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :addressproof, :styles => { :medium => "300x300>",:small=>"50x50" }
  has_attached_file :dobproof, :styles => { :medium => "300x300>",  :small=>"50x50" }

end
