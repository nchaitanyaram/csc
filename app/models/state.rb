#Author: Chaitanya 
#
#==================================
class State < ActiveRecord::Base
  #Relations
  has_many :divisions,:dependent=>:destroy
  has_many :districts,:dependent=>:destroy
  has_many :blocks,:dependent=>:destroy
  
  #Validations
  validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 100}

  #search using meta_where Gem
  def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end

end
