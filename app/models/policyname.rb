#Author: Chaitanya
#Date: June 03 2011
#==============================
class Policyname < ActiveRecord::Base
  #Relations
  
  
  #Validations
  validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 100}
  #search using meta_where Gem
  def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end

end
