#Author: Chaitanya
#Date: 2 June 2011
#================================
class UserProfile < ActiveRecord::Base
  #Relations
  belongs_to :user
  belongs_to :state
  belongs_to :division
  belongs_to :district
  belongs_to :block

  has_many :pancards
  
  #Validations
  validates_presence_of :state_id
  validates_presence_of :division_id
  validates_presence_of :district_id
  #  validates_presence_of :block_id
#  validates_attachment_presence :addressproof                   
#  validates_attachment_presence :idproof
#  validates_attachment_presence :agreement1
#  validates_attachment_presence :agreement2

=begin
 before_save :create_vle_code

  def create_vle_code
     district.increment!(:dis_number)
     self.vle_code =  state.short_code + district.short_code + "%04d" %district.dis_number
  end
=end
  #upload Documents
  has_attached_file :idproof, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :addressproof, :styles => { :medium => "300x300>",:small=>"50x50" }
  has_attached_file :agreement1, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement2, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement3, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement4, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement5, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement6, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement7, :styles => { :medium => "300x300>",  :small=>"50x50" }
  has_attached_file :agreement8, :styles => { :medium => "300x300>",  :small=>"50x50" }
  
  
end
