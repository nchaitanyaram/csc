#Author:Chaitanya
#Date: 26 May 2011
#===================================
class Block < ActiveRecord::Base
  #Relations
  belongs_to :district
  belongs_to :division
  belongs_to :state
  
  #validations
  validates :name, :presence => true, :length => { :maximum => 100}
  validates :district_id,:presence=>true
  
  #search using meta_where Gem
  def self.search(search)
    if search
      where({:name.matches => "%#{search}%"} )
    else
      scoped
    end
  end
  
end
