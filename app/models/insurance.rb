#Author: Chaitanya
#Date: June 5th 2011
#====================================
class Insurance < ActiveRecord::Base
  
  #Relations
  belongs_to :district
  belongs_to :state
  belongs_to :policyname
  belongs_to :user

  #validations
  #validates_uniqueness_of :application_number


  #Insurance Type stored in DB
  INSURANCE_TYPES = [
  [ "New Policy" , "New" ],
  [ "Renewal Policy" , "Renewal" ]
  ]
  
  #Payment Frequency Stored in DB
  PAYMENT_FREQUENY = [
  [ "Annual" , "Annual" ],
  [ "Semi Annual" , "Semi Annual" ],
  [ "Quarterly" , "Quarterly" ],
  [ "Monthly", "Monthly"]
  ]
  
  # AGE = Array.new(8) {|i| 1920+i}
  #search using meta_where Gem
  def self.search(search)
    if search
      where({:transaction_id.matches => "%#{search}%"} | {:transaction_date.matches => "%#{search}%"}  )
    else
      scoped
    end
  end
  
end
