#Author: chaitanya
#====================================
class Role < ActiveRecord::Base
  has_many :assignments
  has_many :users, :through => :assignments

  #search using meta_where Gem
  def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end

end
