#Created By: Chaitanya 
#Date: 25 May 2011
#===================================
class District < ActiveRecord::Base
  #Relations
  belongs_to :division
  belongs_to :state
  has_many :blocks,:dependent=>:destroy
  
  
  #Validations
  validates :name, :presence => true,  :length => { :maximum => 100}
 # validates_uniqueness_of :name  , :scope => :state_id
  
  #search using meta_where Gem
  def self.search(search)
    if search
      where(:name.matches => "%#{search}%")
    else
      scoped
    end
  end  
end

