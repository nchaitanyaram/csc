class UserMailer < ActionMailer::Base
  default :from => "admin@sarkcsc.in"

   def registration_confirmation(user,bulkmail)
    @user = user
    @bulkmail = bulkmail
#    attachments["rails.png"] = File.read("#{Rails.root}/public/images/rails.png")
    mail(:to => "#{user.name} <#{user.email}>", :subject => bulkmail.subject)
  end

end
