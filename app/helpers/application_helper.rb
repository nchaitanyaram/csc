#Author: chaitanya
#===============================
module ApplicationHelper
  ####### per page to select user
  def per_page
    select_tag :per_page, options_for_select([5,10,20,50,100], @per_page.to_i), :onchange => "if(this.value){window.location='?per_page='+this.value+'&page=#{@page}'+'&query=#{@query}'}"
  end
  ##########Sortable
  def sortable(column, title = nil)  
    title ||= column.titleize  
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil  
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"  
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}  
  end  
  #Status of User
  def model_status model
    case model.approved
      when true      
      return "<font color='green'><b>A</b></font>".html_safe        
      when false       
      return "<font color='red'><b>P</b></font>".html_safe    
    end 
  end
  ########## Status Update
  def link_to_model_status model
    html = ''
    case model.approved
      when true
      html << link_to("Block",{:action=>'update_status',:id=>model,:approved=>false}) 
      when false
      html << link_to("Approve", {:action=>'update_status',:id=>model,:approved=>true})
    end
    return html.html_safe
  end
end
