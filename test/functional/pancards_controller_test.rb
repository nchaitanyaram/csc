require 'test_helper'

class PancardsControllerTest < ActionController::TestCase
  setup do
    @pancard = pancards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pancards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pancard" do
    assert_difference('Pancard.count') do
      post :create, :pancard => @pancard.attributes
    end

    assert_redirected_to pancard_path(assigns(:pancard))
  end

  test "should show pancard" do
    get :show, :id => @pancard.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @pancard.to_param
    assert_response :success
  end

  test "should update pancard" do
    put :update, :id => @pancard.to_param, :pancard => @pancard.attributes
    assert_redirected_to pancard_path(assigns(:pancard))
  end

  test "should destroy pancard" do
    assert_difference('Pancard.count', -1) do
      delete :destroy, :id => @pancard.to_param
    end

    assert_redirected_to pancards_path
  end
end
