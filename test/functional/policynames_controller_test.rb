require 'test_helper'

class PolicynamesControllerTest < ActionController::TestCase
  setup do
    @policyname = policynames(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:policynames)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create policyname" do
    assert_difference('Policyname.count') do
      post :create, :policyname => @policyname.attributes
    end

    assert_redirected_to policyname_path(assigns(:policyname))
  end

  test "should show policyname" do
    get :show, :id => @policyname.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @policyname.to_param
    assert_response :success
  end

  test "should update policyname" do
    put :update, :id => @policyname.to_param, :policyname => @policyname.attributes
    assert_redirected_to policyname_path(assigns(:policyname))
  end

  test "should destroy policyname" do
    assert_difference('Policyname.count', -1) do
      delete :destroy, :id => @policyname.to_param
    end

    assert_redirected_to policynames_path
  end
end
