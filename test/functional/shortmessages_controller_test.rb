require 'test_helper'

class ShortmessagesControllerTest < ActionController::TestCase
  setup do
    @shortmessage = shortmessages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shortmessages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shortmessage" do
    assert_difference('Shortmessage.count') do
      post :create, :shortmessage => @shortmessage.attributes
    end

    assert_redirected_to shortmessage_path(assigns(:shortmessage))
  end

  test "should show shortmessage" do
    get :show, :id => @shortmessage.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @shortmessage.to_param
    assert_response :success
  end

  test "should update shortmessage" do
    put :update, :id => @shortmessage.to_param, :shortmessage => @shortmessage.attributes
    assert_redirected_to shortmessage_path(assigns(:shortmessage))
  end

  test "should destroy shortmessage" do
    assert_difference('Shortmessage.count', -1) do
      delete :destroy, :id => @shortmessage.to_param
    end

    assert_redirected_to shortmessages_path
  end
end
