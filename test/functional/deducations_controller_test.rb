require 'test_helper'

class DeducationsControllerTest < ActionController::TestCase
  setup do
    @deducation = deducations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:deducations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create deducation" do
    assert_difference('Deducation.count') do
      post :create, :deducation => @deducation.attributes
    end

    assert_redirected_to deducation_path(assigns(:deducation))
  end

  test "should show deducation" do
    get :show, :id => @deducation.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @deducation.to_param
    assert_response :success
  end

  test "should update deducation" do
    put :update, :id => @deducation.to_param, :deducation => @deducation.attributes
    assert_redirected_to deducation_path(assigns(:deducation))
  end

  test "should destroy deducation" do
    assert_difference('Deducation.count', -1) do
      delete :destroy, :id => @deducation.to_param
    end

    assert_redirected_to deducations_path
  end
end
