require 'test_helper'

class BulkmailsControllerTest < ActionController::TestCase
  setup do
    @bulkmail = bulkmails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bulkmails)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bulkmail" do
    assert_difference('Bulkmail.count') do
      post :create, :bulkmail => @bulkmail.attributes
    end

    assert_redirected_to bulkmail_path(assigns(:bulkmail))
  end

  test "should show bulkmail" do
    get :show, :id => @bulkmail.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @bulkmail.to_param
    assert_response :success
  end

  test "should update bulkmail" do
    put :update, :id => @bulkmail.to_param, :bulkmail => @bulkmail.attributes
    assert_redirected_to bulkmail_path(assigns(:bulkmail))
  end

  test "should destroy bulkmail" do
    assert_difference('Bulkmail.count', -1) do
      delete :destroy, :id => @bulkmail.to_param
    end

    assert_redirected_to bulkmails_path
  end
end
